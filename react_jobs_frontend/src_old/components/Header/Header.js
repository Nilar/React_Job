import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

class Header extends Component {

  constructor(){
    super();
    this.confirmLogout = this.confirmLogout.bind(this);
  }
  confirmLogout(e){
    e.preventDefault();
    var conf = window.confirm("Are you sure to Log Out");
    if(conf){
      this.props.logout();
    }

  }
  render() {
    let links;

    if(this.props.user){
      links = <React.Fragment><li className="nav-item">
          <Link className="nav-link" to="/profile/edit">My Profile</Link>
      </li>
      <li className="nav-item">
      <a onClick={this.confirmLogout} href="#" className="nav-link">Log Out</a>
    </li></React.Fragment>;
      
    }else{
      links = <React.Fragment><li className="nav-item">
            <Link className="nav-link" to="/employers">Employers</Link>
            </li>
            <li className="nav-item">
            <Link className="nav-link" to="/jobseekers">Job Seekers</Link>
            </li></React.Fragment>;
    }

    return (
    <header>
      <nav className="main-nav navbar navbar-expand-lg navbar-dark bg-success">
          <a className="navbar-brand" href="">Awesome Jobs </a>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
            
              <li className="nav-item">
                <Link className="nav-link" to='/'>Home</Link>
              </li>

              <li className="nav-item">
                <Link className="nav-link" to='/jobs'>Jobs</Link>
              </li>

              <li className="nav-item">
                <Link className="nav-link" to='/resumes'>CVs</Link>
              </li>
            </ul>
          </div>

           <div className="my-2 my-lg-0">
           <ul className="navbar-nav">
              {links}
            </ul>
            </div>
        </nav>
    </header>
    );
  }

}

export default Header;
