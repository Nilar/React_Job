import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {

  render() {
    return (
        <footer>
            <p className="text-center"> Copyright &copy; 2018 | Coding Elephant Technology </p>
        </footer>
    );
  }

}

export default Footer;
