import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import commonReducers from './commonReducers';
import categoryReducers from './categoryReducers';
import employerReducers from './employerReducers';
import jobReducers from './jobReducers';
import resumeReducers from './resumeReducers';
import jobseekerReducers from './jobseekerReducers';

export default combineReducers({
	routerRd: routerReducer,
	commonRd: commonReducers,
	catRd:categoryReducers,
	employerRd :employerReducers,
	jobRd :jobReducers,
	resumeRd :resumeReducers,
	jobseekerRd : jobseekerReducers,
});