import{
	GET_CATEGORIES,
	GET_CATEGORIES_OK,
	GET_CATEGORIES_ERR
} from '../config/constants';

const defaultState = {
	inProgress:false,
	categories:null
};

export default (state=defaultState , action)=>{
	switch(action.type){
		case GET_CATEGORIES:
		  return{
		  	...state,
		  	inProgress:true
		  };
		  break;

		case GET_CATEGORIES_OK:
			return{
				...state,
				inProgress:false,
				categories:action.payload.categories || []
			};
		  break;

		case GET_CATEGORIES_ERR:
			return{
				...state,
				inProgress:false
			};
		  break;

		default:
			return state;
	}
}