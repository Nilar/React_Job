import{
	GET_JOBS,
	GET_JOBS_OK,
	GET_JOBS_ERR,
	GET_SALARIES,
	GET_SALARIES_OK,
	GET_SALARIES_ERR

} from '../config/constants';

const defaultState = {
	inProgress:false,
	jobs:null,
	salary:null,
	total_job_pages : 0
};

export default (state=defaultState , action)=>{
	switch(action.type){
		case GET_JOBS:
		  return{
		  	...state,
		  	inProgress:true
		  };
		  break;

		case GET_JOBS_OK:
			return{
				...state,
				inProgress:false,
				jobs:action.payload.jobs || [],
				total_job_pages : action.payload.total_job_pages || 0

			};
		  break;

		case GET_JOBS_ERR:
			return{
				...state,
				inProgress:false
			};
		  break;

		 case GET_SALARIES_OK:
			return{
				...state,
				inProgress:false,
				salaries:action.payload.salaries || []
			};
		  break;

		default:
			return state;
	}
}