import{
	JOBSEEKER_REGISTER,
	JOBSEEKER_REGISTER_OK,
	JOBSEEKER_REGISTER_ERR,
	JOBSEEKER_LOGIN,
	JOBSEEKER_LOGIN_OK,
	JOBSEEKER_LOGIN_ERR
} from '../config/constants';
let u = localStorage.getItem("_user") ? JSON.parse(localStorage.getItem("_user")) : null;
const defaultState = {
	inProgress:false,
	jobseeker:null,
	currentUser : u,
	token : localStorage.getItem("_token")
};

export default (state=defaultState , action)=>{
	switch(action.type){
		case JOBSEEKER_REGISTER:
		  return{
		  	...state,
		  	inProgress:true
		  };
		  

		case JOBSEEKER_REGISTER_OK:
			return{
				...state,
				inProgress:false,
				jobseeker:action.payload.jobseeker || []
			};
		  

		case JOBSEEKER_REGISTER_ERR:
			return{
				...state,
				inProgress:false
			};
		  

		case JOBSEEKER_LOGIN:
		  return{
		  	...state,
		  	inProgress:true
		  };
		  

		case JOBSEEKER_LOGIN_OK:
			return{
				...state,
				inProgress:false,
				currentUser : action.payload.user,
				token : action.payload.token
			};
		  

		case JOBSEEKER_REGISTER_ERR:
			return{
				...state,
				inProgress:false
			};
		// case LOGOUT :
		// return{
		// 		... state,
		// 		currentUser : null,
		// 		token : null
		// 	};

		default:
			return state;
	}
}