import{
	GET_RESUMES,
	GET_RESUMES_OK,
	GET_RESUMES_ERR
} from '../config/constants';

const defaultState = {
	inProgress:false,
	resumes:null,
	total_resume_pages : 0
};

export default (state=defaultState , action)=>{
	switch(action.type){
		case GET_RESUMES:
		  return{
		  	...state,
		  	inProgress:true
		  };
		  break;

		case GET_RESUMES_OK:
			return{
				...state,
				inProgress:false,
				resumes:action.payload.resumes || [],
				total_resume_pages : action.payload.total_pages || 0
			};
		  break;

		case GET_RESUMES_ERR:
			return{
				...state,
				inProgress:false
			};
		  break;

		default:
			return state;
	}
}