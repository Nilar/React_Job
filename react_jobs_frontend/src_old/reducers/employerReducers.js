import{
	GET_EMPLOYERS,
	GET_EMPLOYERS_OK,
	GET_EMPLOYERS_ERR,
	EMPLOYER_LOGIN_OK,
	EMPLOYER_LOGIN,
	GET_EMPLOYER_OK,
	LOGOUT
} from '../config/constants';

let u = localStorage.getItem("_user") ? JSON.parse(localStorage.getItem("_user")) : null;

const defaultState = {
	inProgress:false,
	employers:null,
	employer : null,
	currentUser : u,
	token : localStorage.getItem("_token")
};

export default (state=defaultState , action)=>{
	switch(action.type){
		case GET_EMPLOYERS:
		  return{
		  	...state,
		  	inProgress:true
		  };
	

		case GET_EMPLOYERS_OK:
			return{
				...state,
				inProgress:false,
				//employers:action.payload.employers || []
				currentUser : action.payload.user
			};
		case GET_EMPLOYER_OK:
			return{
				...state,
				inProgress:false,
				employers:action.payload.profile || {}
				
			};
	
		case GET_EMPLOYERS_ERR:
			return{
				...state,
				inProgress:false
			};
		case EMPLOYER_LOGIN :
			return{
					... state,
					inProgress: true
			 };
		case EMPLOYER_LOGIN_OK :
		 return{
				 ... state,
				 inProgress: false,
				 currentUser : action.payload.user,
				 token : action.payload.token
			};
		case LOGOUT :
			return{
					... state,
					currentUser : null,
					token : null
			 };
		 
		default:
			return state;
	}
}