import { applyMiddleware , createStore } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import reducers from './reducers/index';
import { routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

export const history = createHistory();

const myRouterMiddleware = routerMiddleware(history);

const getMiddleware = () => {
	if(process.env.NODE_ENV === 'production'){
		return applyMiddleware(myRouterMiddleware,thunk);
	}else{
		return applyMiddleware(myRouterMiddleware,thunk,createLogger());
	}
};

export const store = createStore(reducers,getMiddleware());