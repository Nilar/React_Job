import axios from 'axios';
import {
	GET_RESUMES,
	GET_RESUMES_OK,
	GET_RESUMES_ERR,
	API_URL,
	ALERT_ERR
} from '../config/constants';

export function serialize(obj){
	var str = [];
	for(var p in obj)
		if(obj.hasOwnProperty(p)){
			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	return str.join("&");
}


export function getResumes(query){
	return function(dispatch) {

		dispatch({type:GET_RESUMES});

		let queryStr = (query) ? serialize(query):"";

		//ajax request
		axios.get(API_URL + "/resumes?"+queryStr)
			.then(res=>{
					dispatch({type:GET_RESUMES_OK,payload:res.data});
				
			})
			.catch(err=>{
				dispatch({type:GET_RESUMES_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}