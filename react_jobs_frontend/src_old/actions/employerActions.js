import axios from 'axios';

import {
	GET_EMPLOYERS,
	GET_EMPLOYERS_OK,
	GET_EMPLOYERS_ERR,
	API_URL,
	ALERT_ERR,
	ALERT_OK,
	EMPLOYER_REGISTER,
	EMPLOYER_REGISTER_OK,
	EMPLOYER_REGISTER_ERR,
	EMPLOYER_LOGIN,
	EMPLOYER_LOGIN_OK,
	EMPLOYER_LOGIN_ERR,
	EMPLOYER_UPDATE,
	EMPLOYER_UPDATE_OK,
	EMPLOYER_UPDATE_ERR,
	LOGOUT
} from '../config/constants';

let httpOptions = {
	headers : { Authorization:"bearer " + localStorage.getItem("_token")}
};

export function getTopEmployers(){
	return function(dispatch) {

		dispatch({type:GET_EMPLOYERS});

		//ajax request
		axios.get(API_URL + "/employers")
			.then(res=>{
					dispatch({type:GET_EMPLOYERS_OK,payload:res.data});
				
			})
			.catch(err=>{
				dispatch({type:GET_EMPLOYERS_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}

export function getEmployers(){
	return function(dispatch) {

		dispatch({type:GET_EMPLOYERS});

		//ajax request
		axios.get(API_URL + "/employers")
			.then(res=>{
					dispatch({type:GET_EMPLOYERS_OK,payload:res.data});
				
			})
			.catch(err=>{
				dispatch({type:GET_EMPLOYERS_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}
export function saveUserSession(user,token){
	return function(dispatch){
		localStorage.setItem("_user",JSON.stringify(user));  // convert obj to string
		localStorage.setItem("_token",token);
	}
}
export function employerLogin(data){
	
	return function(dispatch){
		dispatch({type:EMPLOYER_LOGIN});
		axios.post(API_URL + "/employers/login",data)
		 	.then(res=>{
				 
				if(res.data.token){
					dispatch({type:EMPLOYER_LOGIN_OK,payload:res.data});
					dispatch(saveUserSession(res.data.user,res.data.token));
					window.location="/";
				}
		 	})
		 	.catch(err=>{
		 		dispatch({type:EMPLOYER_LOGIN_ERR,payload:err});
		 		dispatch({type:ALERT_ERR,payload:err});
		 	})
	}
}

export function employerRegister(data){
	return function(dispatch){
		dispatch({type:EMPLOYER_REGISTER});
		axios.post(API_URL + "/employers/register",data)
		 	.then(res=>{
		 		dispatch({type:EMPLOYER_REGISTER_OK,payload:res.data});
		 	})
		 	.catch(err=>{
		 		dispatch({type:EMPLOYER_REGISTER_ERR,payload:err});
		 		dispatch({type:ALERT_ERR,payload:err});
		 	})
	}
}

export function getProfile(){
	return function(dispatch){
		axios.get(API_URL+"/employers/profile",httpOptions)
			.then(res=>{
				//console.log(res.data);
				dispatch({type:GET_EMPLOYERS_OK,payload:res.data});
				localStorage.setItem("_user",JSON.stringify(res.data.user));

			})
			.catch(err=>{
				dispatch({type:ALERT_ERR,payload:err});
			})
	}
}

export function updateProfile(formData){
	return function(dispatch){
		dispatch({type:EMPLOYER_UPDATE});
		axios.post(API_URL+"/employers/profile/",formData,httpOptions)
			.then(res=>{
				//console.log(res.data);
				dispatch({type:EMPLOYER_UPDATE_OK});
				dispatch(getProfile());

			})
			.catch(err=>{
				dispatch({type:EMPLOYER_UPDATE_ERR});
				dispatch({type:ALERT_ERR,payload:err});

			})
	}
}
export function employerLogout(){
	
	return function(dispatch){
		
		axios.post(API_URL+"/employers/logout",{},httpOptions)
			.then(res=>{
				
				dispatch({type:LOGOUT});
				localStorage.removeItem("_user");
				localStorage.removeItem("_token");
				window.location="/"
			})
			.catch(err=>{
				//console.log(err);
			})
	}
}