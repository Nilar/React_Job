import React, { Component } from 'react';
import './ForEmployer.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { employerLogin,employerRegister } from '../../actions/employerActions';

class ForEmployer extends Component {

	constructor(){
		super();
		this.onLoginFormSubmit = this.onLoginFormSubmit.bind(this);
		this.onRegisterFormSubmit = this.onRegisterFormSubmit.bind(this);

		this.state = {
			loginData:{
				email:'',
				password:''
			},
			registerData:{
				name:'',
				phone:'',
				company_name:'',
				company_address:'',
				email:'',
				password:'',
				confirm_password:''
			}
		}
	}

	onLoginFormSubmit(e){
		e.preventDefault();
		let {loginData} = this.state;
		if(!loginData.email || !loginData.password){
			alert("Validation errors!");
			return;
		}
		this.props.login(loginData);
	}

	onRegisterFormSubmit(e){
		e.preventDefault();
		let {registerData} = this.state;
		console.log(registerData);
		if(!registerData.name || !registerData.email){
			alert("Validation errors!");
		}
		this.props.register(registerData);
	}

	onLoginFieldChange(e,field){
		let {loginData} = this.state;
		switch(field){
			case "email":
			loginData.email = e.target.value;
			this.setState({loginData:loginData});
			break;
			case "password":
			loginData.password = e.target.value;
			this.setState({loginData:loginData});
			break;
		}
	}

	onRegisterFieldChange(e,field){
		let {registerData} = this.state;
		switch(field){
			case "name":
			registerData.name = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "phone":
			registerData.phone = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "company_name":
			registerData.company_name = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "company_address":
			registerData.company_address = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "email":
			registerData.email = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "password":
			registerData.password = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "confirm_password":
			registerData.confirm_password = e.target.value;
			this.setState({registerData:registerData});
			break;
		}
	}

  render() {
		
    return (
        <div className="container-fluid foremployer">
        	 <div className="row">
					 		<div className="col-12">
							 	<h1 className="mt-5">Employer find your candidate!</h1>
							</div>
             
           </div>
        	<div className="row">
        		<div className="col-md-6">
        			<div className="container"> 
        				<form action="" method="post" onSubmit={this.onLoginFormSubmit}>
        				  <h4> Login </h4>
						  <div className="form-group">
						    <input type="text" name="email" className="form-control" onChange={(e)=>{this.onLoginFieldChange(e,'email')}}  placeholder="example@gmail.com"/>
						  </div>
						  <div className="form-group">
						    <input type="password" name="password" className="form-control" onChange={(e)=>{this.onLoginFieldChange(e,'password')}}  placeholder="Password"/>
						  </div>
						  <div className="form-group">
						  	<input type="submit" className="btn btn-success" value="Login"/>
						  </div>
						</form>
        			</div>
        		</div>
        		<div className="col-md-6">
        		     <div className="container"> 
        				<form onSubmit = {this.onRegisterFormSubmit}>
        				  <h4> Register </h4>
        				<div className="form-group">
						    <input type="text" name="name" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'name')}}  placeholder="Name"/>
						  </div>
						  <div className="form-group">
						    
						    <input type="email" name="email" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'email')}}  placeholder="example@gmail.com"/>
						  </div>
						   <div className="form-group">
						    
						    <input type="text" name="phone" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'phone')}} placeholder="Phone Number"/>
						  </div>
						  <div className="form-group">
						   
						    <input type="text" className="form-control" name="company_name" onChange={(e)=>{this.onRegisterFieldChange(e,'company_name')}}  placeholder="Company Name"/>
						  </div>
						  <div className="form-group">
						   
						    <input type="text" className="form-control" name="company_address" onChange={(e)=>{this.onRegisterFieldChange(e,'company_address')}} placeholder="Company Address"/>
						  </div>
						   <div className="form-group">
						    
						    <input type="password" className="form-control" name="password" onChange={(e)=>{this.onRegisterFieldChange(e,'password')}}  placeholder="Password"/>
						  </div>
						   <div className="form-group">
						    
						    <input type="password" className="form-control" name="confirm_password" onChange={(e)=>{this.onRegisterFieldChange(e,'confirm_password')}}  placeholder="Confirm Password"/>
						  </div>
						  <div className="form-group">
						  	<input type="submit" className="btn btn-success" value="Register"/>
						  </div>
						</form>
        			</div>
        		</div>
        	</div>
	     </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		login:employerLogin,
		register:employerRegister
	},dispatch)
	
}

export default connect(mapStateToProps,mapDispatchToProps)(ForEmployer);

