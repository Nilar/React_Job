import React, { Component } from 'react';
import './Home.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { sayHi } from '../../actions/commonActions';

import Categories from '../../containers/Categories/Categories';
import PopularCompanies from '../../containers/PopularCompanies/PopularCompanies';
import PopularJobs from '../../containers/PopularJobs/PopularJobs';

class Home extends Component {

constructor(){
	super();
	this.onBtnClick = this.onBtnClick.bind(this);
}


  onBtnClick(){
  	this.props.sayHi();
  }

  render() {
  	if(this.props.message){
  		alert(this.props.message);
  	}
    return (
        <div className="container-fluid">
         {/*  <button onClick={this.onBtnClick}>Click to Say Hi</button>  */} 
	        <Categories />
	        <PopularCompanies />
	        <PopularJobs />
        </div>
    );
  }

}


function mapStateToProps(state){
	return{
		...state,
		message:state.commonRd.message
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
			sayHi:sayHi,
	},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Home);
