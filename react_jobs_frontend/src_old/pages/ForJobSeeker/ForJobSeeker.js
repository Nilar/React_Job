import React, { Component } from 'react';
import './ForJobSeeker.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { jobseekerLogin,jobseekerRegister } from '../../actions/jobseekerActions';

class ForJobSeeker extends Component {

	constructor(){
		super();
		this.onLoginFormSubmit = this.onLoginFormSubmit.bind(this);
		this.onRegisterFormSubmit = this.onRegisterFormSubmit.bind(this);

		this.state = {
			loginData:{
				email:'',
				password:''
			},
			registerData:{
				name:'',
				phone:'',
				career_title:'',
				gender: 1,
				birthday:'',
				password:'',
				confirm_password:''
			}
		}
	}

	onLoginFormSubmit(e){
		e.preventDefault();
		let {loginData} = this.state;
		if(!loginData.email || !loginData.password){
			alert("Validation errors!");
			return;
		}
		this.props.login(loginData);
	}

	onRegisterFormSubmit(e){
		e.preventDefault();
		let {registerData} = this.state;
		
		if(!registerData.career_title || !registerData.phone){
			alert("Validation error!");
			return;
		}
		this.props.register(registerData);  // go to action
	}

	onLoginFieldChange(e,field){
		let {loginData} = this.state;
		switch(field){
			case "email":
			loginData.email = e.target.value;
			this.setState({loginData:loginData});
			break;
			case "password":
			loginData.password = e.target.value;
			this.setState({loginData:loginData});
			break;
		}
	}

	onRegisterFieldChange(e,field){
		let {registerData} = this.state;
		switch(field){
			case "career_title":
			registerData.career_title = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "gender":
			
			registerData.gender = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "birthday":
			registerData.birthday = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "name":
			registerData.name = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "phone":
			registerData.phone = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "password":
			registerData.password = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "confirm_password":
			registerData.confirm_password = e.target.value;
			this.setState({registerData:registerData});
			break;
		}
	}

   render() {
    return (
        <div className="container-fluid foremployer">
        	 <div className="row">
					 		<div className="col-12">
           				<h1 className="mt-5">JobSeeker find your dream jobs</h1>
							</div>
					 </div>
        	<div className="row">
        		<div className="col-md-6">
        			<div className="container"> 
        				<form action="" method="post" onSubmit={this.onLoginFormSubmit}>
        				  <h4> Login </h4>
						  <div className="form-group">
						    <input type="text" name="email" className="form-control" onChange={(e)=>{this.onLoginFieldChange(e,'email')}}  placeholder="Phone No. (xxxxxxxxxxx)"/>
						  </div>
						  <div className="form-group">
						    <input type="password" name="password" className="form-control" onChange={(e)=>{this.onLoginFieldChange(e,'password')}}  placeholder="Password"/>
						  </div>
						  <div className="form-group">
						  	<input type="submit" className="btn btn-success" value="Login"/>
						  </div>
						</form>
        			</div>
        		</div>
        		<div className="col-md-6">
        		     <div className="container"> 
        				<form onSubmit = {this.onRegisterFormSubmit}>
        				  <h4> Register </h4>
        				   
							<div className="form-group">
						    <input type="text" name="career_title" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'career_title')}}  placeholder="Career Title"/>
						  </div>
							<div className="form-group">
						    <label className="pr-5">Gender</label>
								<label className="checkbox-inline pr-5"><input type="radio" value="1" name="gender" onChange={(e)=>{this.onRegisterFieldChange(e,'gender')}} checked /><span className="px-2">Male</span></label>
								<label className="checkbox-inline"><input type="radio" value="0" name="gender" onChange={(e)=>{this.onRegisterFieldChange(e,'gender')}}/><span className="px-2">Female</span></label>
								<div className="clearfix"></div>
						  </div>
							<div className="form-group">
						    <input type="date" name="birthday" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'birthday')}}  placeholder="mm/dd/yyyy"/>
						  </div>
							<div className="form-group">
						    <input type="text" name="name" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'name')}}  placeholder="Name"/>
						  </div>
						  
						   <div className="form-group">
						    <input type="text" name="phone" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'phone')}} placeholder="Phone number"/>
						  </div>
						  <div className="form-group">
						    <input type="password" className="form-control" name="password" onChange={(e)=>{this.onRegisterFieldChange(e,'password')}}  placeholder="Password"/>
						  </div>
						   <div className="form-group">
						    <input type="password" className="form-control" name="confirm_password" onChange={(e)=>{this.onRegisterFieldChange(e,'confirm_password')}}  placeholder="Confirm Password"/>
						  </div>
						  <div className="form-group">
						  	<input type="submit" className="btn btn-success" value="Register"/>
						  </div>
						</form>
        			</div>
        		</div>
        	</div>
	     </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		login:jobseekerLogin,
		register:jobseekerRegister
	},dispatch)
	
}


export default connect(mapStateToProps,mapDispatchToProps)(ForJobSeeker);
