import React, { Component } from 'react';
import './Profile.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { employerLogin,employerRegister } from '../../actions/employerActions';
import  JobSeekerProfile from '../../containers/JobseekerProfile/JobseekerProfile';
import EmployerProfile from '../../containers/EmployerProfile/EmployerProfile';

class Profile extends Component {

	constructor(){
		super();
		this.state = {
			tab : "edit"
		}
	
	}

	componentWillMount(){
		let tab = (this.props.match.params.tab) ? this.props.match.params.tab : "edit";
		this.setState({tab:tab});
	}

	componentWillReceiveProps(newProps){
		if(newProps.match.params.tab != this.state.tab && newProps.match.params.tab != null){
			this.setState({tab:newProps.match.params.tab})
			//alert(newProps.match.params.tab);
		}
	}
	
  render() {
	  
	let content = (this.props.user.role === 0) ? <JobSeekerProfile tab={this.state.tab}/> : <EmployerProfile tab={this.state.tab}/>;

    return (
        <div className="container-fluid foremployer">
        	 <div className="row">
			 	<div className="container">
				 	<h4 className="my-3 text-left">Welcome , <b>{this.props.user.name} !</b></h4>
				 </div>
				
           	</div>
        	<div className="row">
        		{content}
        	</div>
	     </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		user : state.employerRd.currentUser || {}
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		
	},dispatch)
	
}

export default connect(mapStateToProps,mapDispatchToProps)(Profile);

