import React, { Component } from 'react';
import ResumeItems from '../../components/ResumeItem/ResumeItem';
import './Resumes.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import ResumeListing from '../../containers/ResumeListing/ResumeListing';
import { getCategories } from '../../actions/categoryActions';
import { getEmployers } from '../../actions/employerActions';

class Resumes extends Component {

  constructor(){
    super();
    this.state ={
      keyword:'',
      category:'',
      user:''
    }

    this.updateSate = this.updateSate.bind(this);
  }


  updateSate (field , ev)  {
    const state = this.state;
    const newState = Object.assign({},state,{ [field]:ev.target.value});
    this.setState(newState); console.log(this.state);
  } 


  componentWillMount(){
    this.props.getCats();
    this.props.getComs();
  }

  render() {
    let catsOptions ='';
    if(this.props.categories){
      catsOptions = this.props.categories.map((cat,i)=>{
        return <option  key={i} value={cat.id}>{cat.name}</option>;
      });
    }

    let companiesOptions ='';
    if(this.props.companies){
      companiesOptions = this.props.companies.map((com,i)=>{
        return <option  key={i} value={com.id}>{com.company_name}</option>;
      });
    }

    return (
        <div className="container-fluid Resumes">
        <form className="search-form row my-4">
            <div className="col col-md-3">
              <input type="text" name="keyword" onChange={(e)=>{this.updateSate('keyword',e)}} className="form-control" placeholder="Type keyword to search"/>
            </div>
            <div className="col col-md-2">
              <select className="form-control" name="category=" onChange={(e)=>{this.updateSate('category',e)}}>
              <option value="">Category</option>
              <option >Information Category</option>
              { catsOptions }
              </select>
            </div>
            {/*
            <div className="col col-md-2">
              <select className="form-control" name="user"  onChange={(e)=>{this.updateSate('user',e)}}>
              <option value="">Company</option>
              { companiesOptions }
              </select>
            </div>
          */}
            {/*<div className="col col-md-2">
              <button className="btn btn-primary">Search</button>
            </div>*/}
          </form>
        <ResumeListing query={this.state} />
	     </div>
    );
  }

}

function mapStateToProps(state){
  return{
    ...state,
    categories:state.catRd.categories,
    companies:state.employerRd.employers
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    getComs:getEmployers,
    getCats:getCategories
  },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Resumes);
