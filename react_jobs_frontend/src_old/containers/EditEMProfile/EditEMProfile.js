import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {getProfile} from '../../actions/employerActions';
import {updateProfile} from '../../actions/employerActions';
class EditEMProfile extends Component {

    constructor(){
		super();
		this.onUpdateFormSubmit = this.onUpdateFormSubmit.bind(this);

		this.state = {
			userData:{
				name:'',
				phone:'',
				company_name:'',
        company_address:'',
        email : '',
        password:'',
				confirm_password:''
			}
		}
	}

  componentWillMount(){
    this.props.getProfile();
   
    if(this.props.user){
        let {userData} =this.state;
        userData.name = this.props.user.name;
        userData.phone = this.props.user.phone;
        userData.email = this.props.user.email;
        userData.company_name = this.props.user.Employer.company_name;
        userData.company_address = this.props.user.Employer.company_address;
        //userData.email = this.props.user.Employer.email;
        this.setState({userData : userData})
    }
  }

  componentWillReceiveProps(newProps){
        // if(newProps.user){
        //     let {userData} =this.state;
        //     userData.name = newProps.user.name;
        //     this.setState({userData : userData})
        // }
  }
 
  onRegisterFieldChange(e,field){
    let {userData} = this.state;
    
    switch(field){
        case "pname":
        userData.name = e.target.value;
        this.setState({userData:userData});
        break;
        case "phone":
        userData.phone = e.target.value;
        this.setState({userData:userData});
        break;
        case "company_name":
        userData.company_name = e.target.value;
        this.setState({userData:userData});
        break;
        case "company_address":
        userData.company_address = e.target.value;
        this.setState({userData:userData});
        break;
        case "password":
        userData.password = e.target.value;
        this.setState({userData:userData});
        break;
        case "confirm_password":
        userData.confirm_password = e.target.value;
        this.setState({userData:userData});
        break;
        
    }
}
    onUpdateFormSubmit(e){
        e.preventDefault();
        let {registerData} = this.state;
        //console.log(registerData);
        this.props.register(registerData);
    }

    onRegisterFormSubmit(e){
      e.preventDefault();
      //console.log(this.state.userData);
      this.props.updateProfile(this.state.userData);
    }

  render() {

  	return (
        <div className="container">
	        <form onSubmit = {this.onRegisterFormSubmit.bind(this)}>
        			<h4> Upate Profile </h4>
        			<div className="form-group">
						      <input type="text" name="pname" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'pname')}}  placeholder="Name" value={this.state.userData.name}/>
						  </div>
						  <div className="form-group">
						    <input type="email" name="email" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'email')}}  placeholder="example@gmail.com" value={this.state.userData.email}/>
						  </div>
						   <div className="form-group">
						    <input type="text" name="phone" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'phone')}} placeholder="your phone number" value={this.state.userData.phone}/>
						  </div>
						  <div className="form-group">
						    <input type="text" className="form-control" name="company_name" onChange={(e)=>{this.onRegisterFieldChange(e,'company_name')}}  placeholder="your company name" value={this.state.userData.company_name}/>
						  </div>
						  <div className="form-group">
						    <input type="text" className="form-control" name="company_address" onChange={(e)=>{this.onRegisterFieldChange(e,'company_address')}} placeholder="Company Address" value={this.state.userData.company_address}/>
						  </div>
               <div className="form-group">
						    <input type="password" className="form-control" name="password" onChange={(e)=>{this.onRegisterFieldChange(e,'password')}}  placeholder="your password"/>
						  </div>
						   <div className="form-group">
						    <input type="password" className="form-control" name="confirm_password" onChange={(e)=>{this.onRegisterFieldChange(e,'confirm_password')}}  placeholder="your confirm password"/>
						  </div>
						   
						  <div className="form-group">
						  	<input type="submit" className="btn btn-success" value="Update"/>
						  </div>
						</form>
	    </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
        user : state.employerRd.currentUser,
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
    getProfile : getProfile,
    updateProfile : updateProfile
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(EditEMProfile);

