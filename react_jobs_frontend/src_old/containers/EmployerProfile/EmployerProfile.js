import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import  EditEMProfile  from '../../containers/EditEMProfile/EditEMProfile';
import  NewJobs  from '../../containers/NewJobs/NewJobs';
import  MyJobs  from '../../containers/MyJobs/MyJobs';

class EmployerProfile extends Component {

    constructor(){
        super();
        this.state = {
			tab : ""
		}
    }

  componentWillMount(){
   // alert(this.props.tab);
   this.setState({tab:this.props.tab})
  }
  componentWillReceiveProps(newProps){
    if(newProps.tab != this.state.tab){
        this.setState({tab:newProps.tab})
        //alert(newProps.match.params.tab);
    }
}

  render() {
    let t = this.state.tab;
    let tabContent = '';
    if(t=== 'edit'){
        tabContent = <EditEMProfile/>
    }else if(t==='jobs'){
        tabContent = <MyJobs/>
    }else if(t=== 'newjobs'){
        tabContent = <NewJobs/>
    }
  	return (
        <div className="container">
            <div className="row">
	           <div className="col-12">
               <ul className="nav">
                    <li className="nav-item">
                        <Link className={t=="edit" ? "nav-link active" : "nav-link"} to="/profile/edit">Edit Profile</Link>
                    </li>
                    <li className="nav-item">
                        <Link className={t=="jobs" ? "nav-link active" : "nav-link"} to="/profile/jobs">My Job Posts</Link>
                    </li>
                    <li className="nav-item">
                        <Link className={t=="newjobs" ? "nav-link active" : "nav-link"} to="/profile/newjobs">Post a Job</Link>
                    </li>
                </ul>
               </div> 
            </div>
               <div className="row">
                    <div className="col-12">
                        {tabContent}
                    </div>
               </div>
        </div>
	  
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
	
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(EmployerProfile);

