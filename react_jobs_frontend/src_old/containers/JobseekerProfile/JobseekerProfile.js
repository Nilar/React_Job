import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

//import { getCategories } from '../../actions/categoryActions';

class JobseekerProfile extends Component {

  componentWillMount(){
  
  }
 

  render() {

  	return (
        <div className="container">
            <div className="row">
	           <div className="col-12 ">
                <ul className="nav">
                    <li className="nav-item">
                        <Link className="nav-link active" to="/profile/edit">Edit Profile</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/profile/resumes">My Resumes</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/profile/newresume">Upload a Resume</Link>
                    </li>
                </ul>
               </div>
            </div> 
	    </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
	
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(JobseekerProfile);

