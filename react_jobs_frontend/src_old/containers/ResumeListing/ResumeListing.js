import React, { Component } from 'react';
import './ResumeListing.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getResumes } from '../../actions/resumeActions';
import ReactPaginate from 'react-paginate';

class ResumeListing extends Component {

	constructor(props){
		super(props);
		this.state ={
			query:{
				keyword:'',
				category:'',
				page : 1
			}
		};
		this.handlePageClick = this.handlePageClick.bind(this);

	}


	componentWillMount(){ 
		let query = this.props.query || {};
		this.props.getResumes(query);console.log(this.state);
	}

	componentWillReceiveProps(nextProps){ 
		if(nextProps.query != this.state.query){
			this.setState({query:nextProps.query});
			this.props.getResumes(nextProps.query);
		}
	}

	handlePageClick(e){ 
		let page = e.selected + 1;
		let {query} = this.state;
		query.page = page;
		this.setState({query : page});console.log(this.state.query);
		this.props.getResumes(this.state.query);
	}


	render() {
		let resumesHTML = '';
		console.log(this.props.resumes);
		if(!this.props.resumes){
			resumesHTML =<tr><td colSpan="4">Loading...</td></tr>;
		}else if(this.props.resumes.length ==0){
			resumesHTML =<tr><td colSpan="4">No Resume Found</td></tr>;
		}else{
			resumesHTML = this.props.resumes.map((res,i)=>{
				return <tr key={res.id} className="row">
				<td className="col-md-3"><a>{res.title}</a></td>
				<td className="col-md-3"><a>{res.User.name}</a></td>
				<td className="col-md-3"><a>{res.Category.name}</a></td>
				<td className="col-md-3"><a>{res.created_at}</a></td>
				</tr>
			})
		}
		return (
		<div>
		
		<table className="table table-striped">
	       <tbody>
	       { resumesHTML }
	       </tbody> 	
	    </table>
		<ReactPaginate previousLabel={"previous"}
		nextLabel={"next"}
		pageCount={this.props.total_pages}
		marginPagesDisplayed={2}
		pageRangeDisplayed={7}
		onPageChange={this.handlePageClick}
		containerClassName={"pagination justify-content-center"}
		subContainerClassName={"pages pagination"}
		pageClassName = {"page-item"}
		pageLinkClassName = {"page-link"}
		nextClassName ={"page-item"}
		nextLinkClassName = {"page-link"}
		previousClassName={"page-item"}
		previousLinkClassName = {"page-link"}

		activeClassName={"active"} />
		</div>
		);
	}

}

function mapStateToProps(state){
	return{
		...state,
		resumes:state.resumeRd.resumes,
		total_pages : state.resumeRd.total_resume_pages
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		getResumes:getResumes,
	},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ResumeListing);
