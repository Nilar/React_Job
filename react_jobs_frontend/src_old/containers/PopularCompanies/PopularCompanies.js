import React, { Component } from 'react';
import './PopularCompanies.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


import { getTopEmployers } from '../../actions/employerActions';

class PopularCompanies extends Component {

	componentWillMount(){
		this.props.getTopEmployers();
	}

  render() {
  	let employersHtml = "";
  	if(!this.props.employers){
  		employersHtml = <div>Loading...</div>;
  	}else if(this.props.employers.length == 0){
  		employersHtml = <div>No Data Found</div>
  	}else{
  		employersHtml = this.props.employers.map((employer,i)=>{
  			return <div key={employer.id}  className="col-md-2">
	            	<img src="/images/mpt.jpg"/>
	            	<p>12 jobs </p>

	            </div>

  		})
  	}

    return (
        <div className="topemployers row">
	             { employersHtml }     
	    </div>
    );
  }

}

function mapStateToProps(state){
	return {
		...state,
		employers:state.employerRd.employers
	}

}


function mapDispatchToProps(dispatch){
	return bindActionCreators({
		getTopEmployers:getTopEmployers,
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(PopularCompanies);
