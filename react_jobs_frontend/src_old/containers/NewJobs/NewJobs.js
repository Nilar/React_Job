import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { submitJob } from '../../actions/jobActions';

class NewJobs extends Component {

  constructor(){
    super();
    this.state ={
      tab : "",
      jobData: {
        category_id : '',
        title : '',
        salary : '',
        description : '',
        how_to_apply : ''

      }
    }
  }

  componentWillMount(){
  
  }
  onRegisterFieldChange(e,field){
    let {jobData} = this.state;
    
    switch(field){
        case "title":
        jobData.title = e.target.value;
        this.setState({jobData:jobData});
        break;

        case "salary":
        jobData.salary = e.target.value;
        this.setState({jobData:jobData});
        break;

        case "description":
        jobData.description = e.target.value;
        this.setState({jobData:jobData});
        break;

        case "how_to_apply":
        jobData.how_to_apply = e.target.value;
        this.setState({jobData:jobData});
        break;

        case "category_id":
        jobData.category_id = e.target.value;
        this.setState({jobData:jobData});
        break;

        
    }
}
onRegisterFormSubmit(e){
  e.preventDefault();
  this.props.submitJob(this.state.jobData);
}

  render() {

  	return (
      <div className="container">
      <h4> Submit your job offer </h4>
      <form onSubmit = {this.onRegisterFormSubmit.bind(this)}>
              <div className="form-group">
                <select className="form-control" defultValue={this.state.jobData.category_id} onChange={(e)=>{this.onRegisterFieldChange(e,'category_id')}}>
                      <option value="">Choose Category</option>
                      <option value="1">Account</option>
                      <option value="2">Admin</option>
                </select>
              </div>
              
          <div className="form-group">
            <input type="text" name="title" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'title')}}  placeholder="Title" value={this.state.jobData.title}/>
          </div>
          <div className="form-group">
            <input type="text" name="salary" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'salary')}}  placeholder="Salary" value={this.state.jobData.salary}/>
          </div>
           <div className="form-group">
            <textarea name="description" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'description')}} placeholder="Description" value={this.state.jobData.description}></textarea>
          </div>
          <div className="form-group">
            <textarea name="how_to_apply" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'how_to_apply')}} placeholder="How To Apply" value={this.state.jobData.how_to_apply}></textarea>
          </div>
          <div className="form-group">
            <input type="submit" className="btn btn-success" value="Post"/>
          </div>
        </form>
  </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
	
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		submitJob :submitJob
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(NewJobs);

