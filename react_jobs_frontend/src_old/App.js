import React, { Component } from 'react';
import { BrowserRouter as Router, Route , Switch,
  Redirect } from "react-router-dom";
import logo from './logo.svg';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Home from './pages/Home/Home';
import ForEmployer from './pages/ForEmployer/ForEmployer';
import ForJobSeeker from './pages/ForJobSeeker/ForJobSeeker';
import Jobs from './pages/Jobs/Jobs';
import JobDetails from './pages/JobDetails/JobDetails';
import Resumes from './pages/Resumes/Resumes';
import ResumeDetails from './pages/ResumeDetails/ResumeDetails';
import NotFound from './pages/NotFound/NotFound';
import {getProfile} from './actions/employerActions';
import {employerLogout} from './actions/employerActions';
import Profile from './pages/Profile/Profile';


class App extends Component {

  componentWillMount(){
    this.props.getProfile();
  }

  render() {
    return (
      <div className="App">
        <Header user={this.props.user} logout={this.props.employerLogout}/>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/employers" component={ForEmployer} />
          <Route path="/jobseekers" component={ForJobSeeker} />
          <Route path="/resumes" component={Resumes} />
          <Route path="/resume/:id" component={ResumeDetails} />
          <Route path="/jobs" component={Jobs} />
          <Route path="/job/:id" component={JobDetails} />
          <AuthRoute path="/profile/:tab?" component={Profile} />
          <Route path="*" component={NotFound} />
         </Switch>
        <Footer />
       </div>
      //  AuthRoute is private route and :tab? => ? mean optional
    );
  }
}

const token = localStorage.getItem("_token");
const AuthRoute = ({ component : Component, ...rest}) => (
  <Route
    {...rest}
    render={ props =>
    (token!= undefined) ? (
      <Component { ...props} />
    ): (<Redirect
          to={{
            pathname : "/employers/login",
            state : {from: props.location}
          }}
        />
      )
    } 
  />
)

//export default App;
function mapStateToProps(state){
  return{
    ...state,
    user:state.employerRd.currentUser,
    token : state.employerRd.token
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    getProfile : getProfile,
    employerLogout : employerLogout
  },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(App);
