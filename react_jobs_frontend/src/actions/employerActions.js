import axios from 'axios';
import {
	GET_EMPLOYERS,
	GET_EMPLOYERS_OK,
	GET_EMPLOYER_OK,
	GET_EMPLOYERS_ERR,
	API_URL,
	ALERT_ERR,
	ALERT_OK,
	EMPLOYER_REGISTER,
	EMPLOYER_REGISTER_OK,
	EMPLOYER_REGISTER_ERR,
	EMPLOYER_UPDATE,
	EMPLOYER_UPDATE_OK,
	EMPLOYER_UPDATE_ERR,
	EMPLOYER_LOGIN,
	EMPLOYER_LOGIN_OK,
	EMPLOYER_LOGIN_ERR,
	PROFILE_PHOTO_UPDATE,
	PROFILE_PHOTO_UPDATE_OK,
	PROFILE_PHOTO_UPDATE_ERR,
	LOGOUT
} from '../config/constants';

var toastr = require('toastr');

let httpOptions ={
	headers:{ Authorization : "bearer " + localStorage.getItem("_token")}
}


export function getTopEmployers(){
	
	return function(dispatch) {

		dispatch({type:GET_EMPLOYERS});

		//ajax request
		axios.get(API_URL + "/employers",httpOptions)
			.then(res=>{
				//	console.log(res.data.employers);
					dispatch({type:GET_EMPLOYERS_OK,payload:res.data});
				
			})
			.catch(err=>{
				dispatch({type:GET_EMPLOYERS_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}

export function getEmployers(){
	return function(dispatch) {

		dispatch({type:GET_EMPLOYERS});

		//ajax request
		axios.get(API_URL + "/employers")
			.then(res=>{
					dispatch({type:GET_EMPLOYERS_OK,payload:res.data});
				
			})
			.catch(err=>{
				dispatch({type:GET_EMPLOYERS_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}

export function saveUserSession(user,token){
	
	return function(dispatch){
		localStorage.setItem("_user",JSON.stringify(user));
		localStorage.setItem("_token",token);
	}
}

export function employerLogin(data){
	return function(dispatch){
		dispatch({type:EMPLOYER_LOGIN});
		axios.post(API_URL + "/employers/login",data)
		 	.then(res=>{
		 		if(res.data.token){
		 			dispatch({type:EMPLOYER_LOGIN_OK,payload:res.data});
		 			dispatch(saveUserSession(res.data.user,res.data.token));
		 			window.location = "/profile";
		 		}
		 	})
		 	.catch(err=>{
				 if(err.response){
					toastr.warning(err.response.data.message,"Error!",{timeOut:5000})
				 }
		 		dispatch({type:EMPLOYER_LOGIN_ERR,payload:err});
		 		dispatch({type:ALERT_ERR,payload:err});
		 	})
	}
}

export function employerRegister(data){
	return function(dispatch){
		dispatch({type:EMPLOYER_REGISTER});
		axios.post(API_URL + "/employers/register",data)
		 	.then(res=>{
		 		dispatch({type:EMPLOYER_REGISTER_OK,payload:res.data});
		 		window.reload();
		 	})
		 	.catch(err=>{
		 		dispatch({type:EMPLOYER_REGISTER_ERR,payload:err});
		 		dispatch({type:ALERT_ERR,payload:err});
		 	})
	}
}

export function getProfile(){
	return function(dispatch){
		axios.get(API_URL + "/employers/profile",httpOptions)
			.then(res=>{
				if(res.data.user){
					dispatch({type:GET_EMPLOYER_OK,payload:res.data});
					localStorage.setItem("_user",JSON.stringify(res.data.user));
				}
			})
			.catch(err=>{
				dispatch({type:ALERT_ERR,payload:err});
			})
	}
}

export function updateProfile(formData){
	return function(dispatch){
		dispatch({type:EMPLOYER_UPDATE});
		axios.post(API_URL + "/employers/profile",formData,httpOptions)
			.then(res=>{
				dispatch({type:EMPLOYER_UPDATE_OK});
				dispatch(getProfile());
				
			})
			.catch(err=>{
				dispatch({type:EMPLOYER_UPDATE_ERR});
				dispatch({type:ALERT_ERR,payload:err});
			})
	}
}

export function updateProfilePhoto(file){
	return function(dispatch){
		dispatch({type:PROFILE_PHOTO_UPDATE});

		let httpOpts ={
			headers:{ Authorization : "bearer " + localStorage.getItem("_token"),
			'Content-Type':'multipart/form-data'}
		}

		let data = new FormData();
		data.append("photo",file,file.name);

		axios.post(API_URL + "/employers/profile_photo",data,httpOpts)
			.then(res=>{
				dispatch({type:PROFILE_PHOTO_UPDATE_OK});
				//window.location.reload();
				dispatch(getProfile());
			})
			.catch(err=>{
				dispatch({type:PROFILE_PHOTO_UPDATE_ERR});
				dispatch({type:ALERT_ERR,payload:err});
			})
	}
}

export function	employerLogout(){
	return	function(dispatch){
		dispatch({type:LOGOUT});
		localStorage.removeItem("_user");
		localStorage.removeItem("_token");
		window.location = "/";
		axios.post(API_URL + "/employers/logout",{},httpOptions)
				.then(res=>{
					console.log(res.data);
					
				})
				.catch(err=>{
					console.log(err);
				});
	}
}