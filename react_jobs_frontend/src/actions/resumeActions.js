import axios from 'axios';
import {
	GET_RESUMES,
	GET_RESUMES_OK,
	GET_RESUMES_ERR,
	POST_RESUME,
	POST_RESUME_OK,
	POST_RESUME_ERR,
	RESUME_PDF_UPDATE,
	RESUME_PDF_UPDATE_OK,
	RESUME_PDF_UPDATE_ERR,
	GET_MYRESUME,
	GET_MYRESUME_OK,
	GET_MYRESUME_ERR,
	API_URL,
	ALERT_ERR
} from '../config/constants';

let httpOptions ={
	headers:{ Authorization : "bearer " + localStorage.getItem("_token")}
}

export function serialize(obj){
	var str = [];
	for(var p in obj)
		if(obj.hasOwnProperty(p)){
			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	return str.join("&");
}


export function getResumes(query){
	return function(dispatch) {

		dispatch({type:GET_RESUMES});

		let queryStr = (query) ? serialize(query):"";

		//ajax request
		axios.get(API_URL + "/resumes?"+queryStr)
			.then(res=>{
					dispatch({type:GET_RESUMES_OK,payload:res.data});
				
			})
			.catch(err=>{
				dispatch({type:GET_RESUMES_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}

export function getMyResume(query){
	
	return function(dispatch){
		dispatch({type:GET_MYRESUME});

		let queryStr = (query) ?  serialize(query) : "";
		
		axios.get(API_URL + "/resumes/mine?"+ queryStr,httpOptions)
		.then(res=>{
			dispatch({type:GET_MYRESUME_OK,payload:res.data});
		})
		.catch(err=>{
			dispatch({type:GET_MYRESUME_ERR,payload:err});
			dispatch({type:ALERT_ERR,payload:err});
		})

	}
}

export function submitResume(formData){

	return function(dispatch){
		dispatch({type: POST_RESUME});
		axios.post(API_URL +'/resumes',formData,httpOptions)
		.then(res=>{
			dispatch({ type : POST_RESUME_OK,payload:res.data});
		})
		.catch(err => {
			dispatch({ type : POST_RESUME_ERR,payload:err});
			dispatch({type:ALERT_ERR,payload:err});
		})

	}
}

export function uploadResumefile(file){
	let httpOption ={
		headers:{ Authorization : "bearer " + localStorage.getItem("_token"),'Content-Type':'multipart/form-data'}
	}
	let data = new FormData();
	console.log("file.name");
	console.log(file.name);
	data.append("photo",file,file.name);
	return function(dispatch){
		dispatch({type: RESUME_PDF_UPDATE});
		axios.post(API_URL + '/resumes/uploadresumefile',data,httpOption)
		.then(res=>{
			dispatch({ type : RESUME_PDF_UPDATE_OK});
		})
		.catch(err=>{
			dispatch({ type : RESUME_PDF_UPDATE_ERR,payload:err});
			dispatch({type:ALERT_ERR,payload:err});
		})
	}
}