import axios from 'axios';
import { push } from 'react-router-redux';
import {
	GET_JOBS,
	GET_JOBS_OK,
	GET_JOBS_ERR,
	GET_SALARIES,
	GET_SALARIES_OK,
	GET_SALARIES_ERR,
	POST_JOB,
	POST_JOB_OK,
	POST_JOB_ERR,
	GET_MYJOBS,
	GET_MYJOBS_OK,
	GET_MYJOBS_ERR,
	API_URL,
	ALERT_ERR
} from '../config/constants';

let httpOptions ={
	headers:{ Authorization : "bearer " + localStorage.getItem("_token")}
}

export function serialize(obj){
	var str = [];
	for(var p in obj)
		if(obj.hasOwnProperty(p)){
			str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	return str.join("&");
}

export function getJobs(query){
	return function(dispatch) {

		dispatch({type:GET_JOBS});

		let queryStr = (query) ? serialize(query):"";

		//ajax request
		axios.get(API_URL + "/jobs?"+queryStr)
			.then(res=>{
				dispatch({type:GET_JOBS_OK,payload:res.data});
			})
			.catch(err=>{
				dispatch({type:GET_JOBS_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}

export function getMyJobs(query){ 
	return function(dispatch) {

		dispatch({type:GET_MYJOBS});

		let queryStr = (query) ? serialize(query):"";

		//ajax request
		axios.get(API_URL + "/jobs/mine?"+queryStr,httpOptions)
			.then(res=>{
	
				dispatch({type:GET_MYJOBS_OK,payload:res.data});
				
			})
			.catch(err=>{
				dispatch({type:GET_MYJOBS_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}

export function getSalaries(query){
	return function(dispatch) {

		dispatch({type:GET_JOBS});

		//ajax request
		axios.get(API_URL + "/jobs/salaries")
			.then(res=>{
				dispatch({type:GET_SALARIES_OK,payload:res.data});
			})
			.catch(err=>{
				dispatch({type:GET_SALARIES_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});
			});

	}
}
export function submitJob(formData){
	return function(dispatch){
		dispatch({type:POST_JOB});
		axios.post(API_URL + "/jobs",formData,httpOptions)
			.then(res=>{
				dispatch({type:POST_JOB_OK,payload:res.data});
				dispatch(push("/profile/jobs"));
			})
			.catch(err=>{
				dispatch({type:POST_JOB_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});
			})
	}
}
