import axios from 'axios';
import { push } from 'react-router-redux';

import {
	API_URL,
	ALERT_ERR,
	ALERT_OK,
	JOBSEEKER_REGISTER,
	JOBSEEKER_REGISTER_OK,
	JOBSEEKER_REGISTER_ERR,
	JOBSEEKER_LOGIN,
	JOBSEEKER_LOGIN_OK,
	JOBSEEKER_LOGIN_ERR,
	JOBSEEKER_UPDATE_OK,
	JOBSEEKER_UPDATE,
	JOBSEEKER_UPDATE_ERR
} from '../config/constants';
import { getProfile } from '../actions/employerActions';

let httpOptions ={
	headers:{ Authorization : "bearer " + localStorage.getItem("_token")}
}

export function saveUserSession(user,token){
	return function(dispatch){
		localStorage.setItem("_user",JSON.stringify(user));
		localStorage.setItem("_token",token);
	}
}

export function jobseekerLogin(data){
	return function(dispatch){
		dispatch({type:JOBSEEKER_LOGIN});
		axios.post(API_URL + "/jobseekers/login",data)
		 	.then(res=>{
		 		if(res.data.token){
		 			dispatch({type:JOBSEEKER_LOGIN_OK,payload:res.data});
		 			dispatch(saveUserSession(res.data.user,res.data.token));
		 			window.location = "/";
		 		}
		 	})
		 	.catch(err=>{
		 		dispatch({type:JOBSEEKER_LOGIN_ERR,payload:err});
		 		dispatch({type:ALERT_ERR,payload:err});
		 	})
	}
}

export function jobseekerRegister(data){
	return function(dispatch){
		dispatch({type:JOBSEEKER_REGISTER});
		axios.post(API_URL + "/jobseekers/register",data)
		 	.then(res=>{
				 dispatch({type:JOBSEEKER_REGISTER_OK,payload:res.data});
				// history.push('/jobseekers')
		 	})
		 	.catch(err=>{
		 		dispatch({type:JOBSEEKER_REGISTER_ERR,payload:err});
		 		dispatch({type:ALERT_ERR,payload:err});
		 	})
	}
}

export function updateProfile(formData){
	return function(dispatch){
		dispatch({type:JOBSEEKER_UPDATE});

		axios.post(API_URL + "/employers/profile",formData,httpOptions)
			.then(res=>{
				dispatch({type:JOBSEEKER_UPDATE_OK});
				dispatch(getProfile());
			})
			.catch(err=>{
				dispatch({type:JOBSEEKER_UPDATE_ERR});
				dispatch({type:ALERT_ERR,payload:err});
			})
	}
}