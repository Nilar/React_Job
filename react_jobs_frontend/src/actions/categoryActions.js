import axios from 'axios';
import {
	GET_CATEGORIES,
	GET_CATEGORIES_OK,
	GET_CATEGORIES_ERR,
	API_URL,
	ALERT_ERR
} from '../config/constants';


export function getCategories(){
	return function(dispatch) {

		dispatch({type:GET_CATEGORIES});

		//ajax request
		axios.get(API_URL + "/categories")
			.then(res=>{
					dispatch({type:GET_CATEGORIES_OK,payload:res.data});
				
			})
			.catch(err=>{
				dispatch({type:GET_CATEGORIES_ERR,payload:err});
				dispatch({type:ALERT_ERR,payload:err});

			});

	}
}