import React, { Component } from 'react';
import { BrowserRouter as Router, Route , Switch,
  Redirect } from "react-router-dom";
import logo from './logo.svg';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getProfile } from './actions/employerActions';
import { employerLogout } from './actions/employerActions';
import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Home from './pages/Home/Home';
import ForEmployer from './pages/ForEmployer/ForEmployer';
import ForJobSeeker from './pages/ForJobSeeker/ForJobSeeker';
import Jobs from './pages/Jobs/Jobs';
import JobDetails from './pages/JobDetails/JobDetails';
import Resumes from './pages/Resumes/Resumes';
import ResumeDetails from './pages/ResumeDetails/ResumeDetails';
import Profile from './pages/Profile/Profile';
import NotFound from './pages/NotFound/NotFound';
import Company from './pages/Company/Company';


class App extends Component {
  componentWillMount(){
    this.props.getProfile();
  }
  render() {
    return (
      <div className="App">
        <Header logout={this.props.employerLogout} user={this.props.user}/>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/employers" component={ForEmployer} />
          <AuthRoute path="/profile/:tab?" component={Profile} />
          <Route path="/jobseekers" component={ForJobSeeker} />
          <Route path="/resumes" component={Resumes} />
          <Route path="/resume/:id" component={ResumeDetails} />
          <Route path="/jobs/:category?" component={Jobs} />
          <Route path="/companies/:id?" component={Company} />
          <Route path="/job/:id" component={JobDetails} />
          <Route path="*" component={NotFound} />
         </Switch>
        <Footer />
       </div>
    );
  }
}

const token= localStorage.getItem('_token');


    // localStorage.removeItem("_user");
		// localStorage.removeItem("_token");
const AuthRoute = ({ component: Component,...rest})=>(
  <Route 
    {...rest}
    render={props => 
      (token != undefined ) ? (
        <Component {...props}/>
      ) : (
        <Redirect 
         to={{
           pathname : "/employers/login",
           state:{ from: props.location}
         }}
        />
      )
    }
  />
)

function mapStateToProps(state){
  return{
    ...state,
    user : state.employerRd.currentUser,
    token : state.employerRd.token
  };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    getProfile:getProfile,
    employerLogout:employerLogout
  },dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(App);
