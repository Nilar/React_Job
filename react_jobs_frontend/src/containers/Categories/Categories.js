import React, { Component } from 'react';
import './Categories.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getCategories } from '../../actions/categoryActions';
import { Link } from 'react-router-dom';

class Categories extends Component {

  componentWillMount(){
  	this.props.getCats();
  }
 

  render() {

  	let categoriesHtml = '';
	  if(!this.props.categories){
	  	categoriesHtml = <div className="col-md-3">Loading...</div>;
	  }else if(this.props.categories.length ==0){
	  	categoriesHtml =<div className="col-md-12">No Categories Found</div>;
	  }else{
	  	categoriesHtml = this.props.categories.map((cat,i)=>{
	  		
	  		return <div  key={cat.id} className="col-md-3">
	            	<p> 
	            		<Link key={i} to={"/jobs/" + cat.id }>{cat.name}</Link><span>(556)</span>
	            	</p>
	              </div>
	  	})
	  }

    return (
        <div className="categories row mx-2">
	            { categoriesHtml }
	    </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		categories:state.catRd.categories
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		getCats:getCategories,
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(Categories);

