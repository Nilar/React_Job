import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { getMyJobs } from '../../actions/jobActions';
import ReactPaginate from 'react-paginate';
var moment = require('moment');

class MyJobs extends Component {

constructor(){
  super();
  this.state = {
      tab: "",
      query:{
        page : 1
      }
  }
  /*this.handlePageClick = this.handlePageClick.bind(this);*/
}

componentWillMount(){
  this.props.getMyJobs(this.state.query);
}

componentWillReceiveProps(newProps){
    if(newProps.tab != this.state.tab){
    this.setState({tab:newProps.tab});
    }
}

/*handlePageClick(e){ 
    let page = e.selected + 1;
    let {query} = this.state;
     query.page = page;
    this.setState({query : page});console.log(this.state.query);
    this.props.getJobs(this.state.query);
  }*/

render() {

   let jobsHtml = '';
    if(!this.props.jobs){
      jobsHtml = <tr><td colSpan="4">Loading...</td></tr>;
    }else if(this.props.jobs.length ==0){
      jobsHtml =<tr><td colSpan="4">No Jobs Found</td></tr>;
    }else{
      jobsHtml = this.props.jobs.map((job,i)=>{
        let mt = moment(job.created_at).fromNow();
        return  <tr key={job.id}>
            <td>{job.title}</td>
             <td>{job.salary}</td>
             <td>{job.company}</td>
             <td>{job.description}</td>
             <td>{mt}</td>
             </tr>
      })
    }

    return (
    <div className="container">
        <h3 className="pt-4">My Job Posts</h3>
        <div>
           <table className="table table-striped">
           <tbody>
           { jobsHtml }
           </tbody>   
        </table>
         <ReactPaginate previousLabel={"previous"}
                       nextLabel={"next"}
                       pageCount={this.props.total_pages}
                       marginPagesDisplayed={1}
                       pageRangeDisplayed={7}
                       onPageChange={this.handlePageClick}
                       containerClassName={"pagination justify-content-center"}
                       subContainerClassName={"pages pagination"}
                       pageClassName = {"page-item"}
                       pageLinkClassName = {"page-link"}
                       nextClassName ={"page-item"}
                       nextLinkClassName = {"page-link"}
                       previousClassName={"page-item"}
                       previousLinkClassName = {"page-link"}

            activeClassName={"active"} />
        </div>
        
    </div>
    );
  }
}

function mapStateToProps(state){
	return {
    ...state,
    jobs:state.jobRd.jobs,
    total_pages : state.jobRd.total_job_pages
  
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
          getMyJobs:getMyJobs  
	},dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyJobs);
//export default MyJobs;
