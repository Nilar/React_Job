import React, { Component } from 'react';
import './PopularCompanies.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getTopEmployers } from '../../actions/employerActions';
import {Link} from 'react-router-dom';
class PopularCompanies extends Component {

	componentWillMount(){
		this.props.getTopEmployers();
	}

  render() {
	  let employersHtml = "";
	  console.log(this.props.employers);
  	if(!this.props.employers){
  		employersHtml = <div>Loading...</div>;
  	}else if(this.props.employers.length == 0){
  		employersHtml = <div>No Data Found</div>
  	}else{
  		employersHtml = this.props.employers.map((employer,i)=>{
  			return <div key={i} className="col-md-2">
			  		<Link to={"/companies/"+employer.id}>
	            	<p className="mx-auto d-block"><img src={employer.company_logo}/></p>
	            	<p className="text-info">{employer.company_name} </p>
					<p className="text-danger">{employer.total_jobs || 0} </p>
					</Link>

	            </div>

  		})
  	}

    return (
        <div className="topemployers row">
	             { employersHtml }     
	    </div>
    );
  }

}

function mapStateToProps(state){
	return {
		...state,
		employers:state.employerRd.employers
	}

}


function mapDispatchToProps(dispatch){
	return bindActionCreators({
		getTopEmployers:getTopEmployers,
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(PopularCompanies);
