import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getProfile } from '../../actions/employerActions';
import { updateProfile } from '../../actions/jobseekerActions';

import {Link} from 'react-router-dom';
import $ from "jquery";

class EditJSProfile extends Component {

	constructor(){
		super();
		this.onFormSubmit = this.onFormSubmit.bind(this);

		this.state = {
			JsData:{
					name:'',
					phone:'',
					career_title:'',
					gender:'',
					birthday : '',
					password:'',
					confirm_password:''
				
			}
		}
	}

	componentWillMount(){
		this.props.getProfile();
		if(this.props.user){ console.log(this.props.user);
			let { JsData } = this.state;
			JsData.name = this.props.user.name;
			JsData.phone = this.props.user.phone;
			JsData.career_title = this.props.user.Jobseeker.career_title;
			JsData.gender = this.props.user.Jobseeker.gender;
			JsData.birthday = this.props.user.Jobseeker.birthday || '';
			this.setState({JsData : JsData});
		}
	}

	componentWillReceiveProps(newProps){
		/*if(newProps.user){
			let { JsData } = this.state;
			JsData.name = newProps.user.name
			this.setState({JsData : JsData});
		}*/
	}

	onFormSubmit(e){
		e.preventDefault();
		this.props.updateProfile(this.state.JsData);
		$('#hideMe').fadeOut(5000);
	}

	onFieldChange(e,field){
		let {JsData} = this.state;
		switch(field){
			case "name":
			JsData.name = e.target.value;
			this.setState({JsData:JsData});
			break;
			case "phone":
			JsData.phone = e.target.value;
			this.setState({JsData:JsData});
			break;
			case "career_title":
			JsData.career_title = e.target.value;
			this.setState({JsData:JsData});
			break;
			case "gender":
			JsData.gender = e.target.value;
			this.setState({JsData:JsData});
			break;
			case "birthday":
			JsData.birthday = e.target.value;
			this.setState({JsData:JsData});
			break;
			case "password":
			JsData.password = e.target.value;
			this.setState({JsData:JsData});
			break;
			case "confirm_password":
			JsData.confirm_password = e.target.value;
			this.setState({JsData:JsData});
			break;
		}
	}

	render() {
		let t = this.state.tab;
		let updatetxt = this.props.inProgress ? "Processing" : "Update";
		return (
			<div className="container">
			<h3 className="pt-3">Update Your Profile</h3>
			<div className="text-success py-2" id="hideMe">{this.props.update}</div>
			<form onSubmit = {this.onFormSubmit.bind(this)}>
			
				<div className="form-group">
				<input type="text" name="career_title" className="form-control" onChange={(e)=>{this.onFieldChange(e,'career_title')}} placeholder="your career" value={this.state.JsData.career_title}/>
				</div>
				<div className="form-group">
					<label className="form-check-inline">
						<input type="radio" className="form-check-input" onChange={(e)=>{this.onFieldChange(e,'gender')}}  name="gender" value="1" checked={this.state.JsData.gender == 1}/>Male
					</label>
					<label className="form-check-inline">
						<input type="radio" className="form-check-input" onChange={(e)=>{this.onFieldChange(e,'gender')}}  name="gender" value="0" checked={this.state.JsData.gender == 0}/>Female
					</label>
				</div>
				<div className="form-group">
				<input type="date" name="birthday" className="form-control" onChange={(e)=>{this.onFieldChange(e,'birthday')}} placeholder="Enter birthday" value={this.state.JsData.birthday}/>
				</div>
				<div className="form-group">
				<input type="text" name="name" className="form-control" onChange={(e)=>{this.onFieldChange(e,'name')}}  placeholder="Enter Your Name" value={this.state.JsData.name}/>
				</div>
				<div className="form-group">
				<input type="text" name="phone" className="form-control" onChange={(e)=>{this.onFieldChange(e,'phone')}} placeholder="your phone number" value={this.state.JsData.phone}/>
				</div>
				<div className="form-group">
				<input type="password" className="form-control" name="password" onChange={(e)=>{this.onFieldChange(e,'password')}}  placeholder="your password" value={this.state.JsData.password}/>
				</div>
				<div className="form-group">
				<input type="password" className="form-control" name="confirm_password" onChange={(e)=>{this.onFieldChange(e,'confirm_password')}}  placeholder="your confirm password" value={this.state.JsData.confirm_password}/>
				</div>
				<div className="form-group">
				<input type="submit" className="btn btn-success" value={updatetxt}/>
				</div>
			</form>
			</div>
			);
		}

	}

	function mapStateToProps(state){
		return{
			...state,
			user : state.employerRd.currentUser, // employerRD have jobseeker and employer
			update : state.jobseekerRd.update
		}

	}

	function mapDispatchToProps(dispatch){
		return bindActionCreators({
			getProfile : getProfile,
			updateProfile : updateProfile
		},dispatch)

	}

	export default connect(mapStateToProps,mapDispatchToProps)(EditJSProfile);

