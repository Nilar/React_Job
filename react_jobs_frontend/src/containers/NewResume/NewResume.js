import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { submitResume } from '../../actions/resumeActions';
import { uploadResumefile} from '../../actions/resumeActions';
import { getCategories } from '../../actions/categoryActions';
import $ from 'jquery';
class NewResume extends Component {

constructor(){
  super();
  this.state = {
      tab: "",
      resumeData: {
         title:'',
         description:'',
         photo:'',
         how_to_contact:'',
         category_id:'',
         savemsg : ''
      }
  }
}

componentWillMount(){
  this.props.getCategories();
}

componentWillReceiveProps(newProps){
    // if(newProps.tab != this.state.tab){
    // this.setState({tab:newProps.tab});
    // }
}


onFieldChange(e, field){
    let {resumeData} = this.state;
    
    switch (field){
        case "title":
        resumeData.title = e.target.value;
        this.setState({resumeData:resumeData});
        break;

        case "how_to_contact":
        resumeData.how_to_contact = e.target.value;
        this.setState({resumeData:resumeData});
        break;

        case "description":
        resumeData.description = e.target.value;
        this.setState({resumeData:resumeData});
        break;

        case "how_to_apply":
        resumeData.how_to_apply = e.target.value;
        this.setState({resumeData:resumeData});
        break;

        case "category_id":
        resumeData.category_id = e.target.value;
        this.setState({resumeData:resumeData});
        break;

    }
 }

 onFormSubmit(e){
     e.preventDefault();
     //todo validation 
    this.props.submitResume(this.state.resumeData);
    $('#hideMe').fadeOut(5000);
 }
 uploadResume(e){
    e.preventDefault();
    console.log(e.target.files);
    let file = e.target.files || e.datatransfer.files;
    if(!file.length){
        console.log("no upload");
    }
    if(file[0]){
        this.props.uploadResumefile(file[0]);
    }
 }
render() {
    let capOptions = this.props.categories.map((cat,i)=>{
      return <option key={cat.id} value={cat.id}>{cat.name}</option>
    });
    return (
    <div className="container">
    <h3 className="pt-4">Submit your Resume</h3>
        <div className="text-success py-2" id="hideMe">{this.props.savemsg}</div>
        <form action="" method="POST" onSubmit={this.onFormSubmit.bind(this)}>
                    <div className="form-group">
                        <select className="form-control" defaultValue={this.state.resumeData.category_id} onChange={(e) => this.onFieldChange(e,"category_id")} >
                          <option value="">Choose Category</option>
                          { capOptions }
                        </select>
                    </div>

                    <div className="form-group">
                        <input type="text" className="form-control" id="title" value={this.state.resumeData.title} onChange={(e) => this.onFieldChange(e,"title")}   placeholder="Title" />
                    </div>
                    <div className="form-group">
                        <textarea className="form-control" id="description" value={this.state.resumeData.description} onChange={(e) => this.onFieldChange(e,"description")}   placeholder="Job Description"></textarea>
                    </div>
                    <div className="form-group">
                        <textarea className="form-control" id="how_to_contact" value={this.state.resumeData.how_to_contact} onChange={(e) => this.onFieldChange(e,"how_to_contact")}   placeholder="How to Contact" ></textarea>
                    </div>
                    <div className="form-group">
                        <input type="file" name="photo" className="form-control" onChange ={this.uploadResume.bind(this)} value={this.state.resumeData.photo} />
                      
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
        </form>
        
    </div>
    );
  }
}

function mapStateToProps(state){
	return {
    ...state,
    categories : state.catRd.categories || [],
    savemsg : state.resumeRd.savemsg
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
        submitResume: submitResume,
        getCategories : getCategories,
        uploadResumefile : uploadResumefile
        
	},dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewResume);
//export default NewJob;
