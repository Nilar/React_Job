import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {Link} from 'react-router-dom';
import  EditJSProfile  from '../../containers/EditJSProfile/EditJSProfile';
import  NewResume  from '../../containers/NewResume/NewResume';
import  MyResume  from '../../containers/MyResume/MyResume';


class JobSeekerProfile extends Component {
	
	constructor(){
		super();
		this.state = {
			tab : ""
		}
	}

  componentWillMount(){
		this.setState({tab:this.props.tab});
  }
	componentWillReceiveProps(newProps){
    if(newProps.tab != this.state.tab){
      this.setState({ tab : newProps.tab});
    }
  }

  render() {

		let t = this.state.tab; 

  	let tabContent = '';
  	if(t == "edit"){
  		tabContent = <EditJSProfile/>;
		}
		else if(t == "myresume"){
  		tabContent = <MyResume/>;
  	}else if(t == "newresume"){
  		tabContent = <NewResume/>;
  	}

    return (
			<div className="container">
        <div className="row">
						<div className="col-12">
								<ul className="nav nav-tabs">
									<li className="nav-item">
											<Link className={ t == "edit"  ? "nav-link active" : "nav-link" } to="/profile/edit">Edit Profile</Link>
									</li>
									<li className="nav-item">
										<Link className={ t == "myresume"  ? "nav-link active" : "nav-link" } to="/profile/myresume">My Resumes</Link>
									</li>
									<li className="nav-item">
										<Link className={ t == "newresume"  ? "nav-link active" : "nav-link" } to="/profile/newresume">Upload a Resume</Link>

									</li>
								</ul>
							</div>
	        </div> 
					<div className="row">
		        <div className="col-12">
		         { tabContent }
		        </div>
		    </div> 
	    </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		user:state.catRd.JobSeekerProfile
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(JobSeekerProfile);

