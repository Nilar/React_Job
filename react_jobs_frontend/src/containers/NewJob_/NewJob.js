import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {Link} from 'react-router-dom';

class NewJob extends Component {

	constructor(){
		super();
		this.state = {
			tab : '',
			jobData: {
				title : '',
				salary : '',
				description : '',
				how_to_apply:'',
				category_id : ''
			}
		}
	}

 

	  componentWillReceiveProps(newProps){
    if(newProps.tab != this.state.tab){
      this.setState({ tab : newProps.tab});
    }
  }

  	onFieldChange(e,field){
		let {jobData} = this.state;
		console.log(this.state);
		switch(field){
			case "title":
			jobData.title = e.target.value;
			this.setState({jobData:jobData});
			break;
			case "salary":
			jobData.salary = e.target.value;
			this.setState({jobData:jobData});
			break;
			case "description":
			jobData.description = e.target.value;
			this.setState({jobData:jobData});
			break;
			case "how_to_apply":
			jobData.how_to_apply = e.target.value;
			this.setState({jobData:jobData});
			break;
			case "category_id":
			jobData.category_id = e.target.value;
			this.setState({jobData:jobData});
			break;
		}
	}
  
  render() {
  	let t = this.state.tab;
    return (
       
			<div className="container">
			<h3>Update Your Profile</h3>
			<form>
			<div className="form-group">
				<select className="form-control" defaultValue={this.state.jobData.category_id} onChange={(e)=>{this.onRegisterFieldChange(e,'category_id')}}>
					<option> Choose Category </option>
					<option value="1">Account</option>
					<option value="2">Engineer</option>
				</select>
			</div>
			<div className="form-group">
			<label>Title</label>
			<input type="text" name="title" className="form-control" value={this.state.jobData.title } onChange={(e)=>{this.onFieldChange(e,'title')}}  placeholder="Enter Name"/>
			</div>
			<div className="form-group">
			<label>Salary</label>
			<input type="text" name="salary" className="form-control" value={this.state.jobData.salary } onChange={(e)=>{this.onFieldChange(e,'salary')}}  placeholder="Enter Email"/>
			</div>
			<div className="form-group">
			<label>description</label>
			<textarea  name="description" className="form-control" value={this.state.jobData.description } onChange={(e)=>{this.onFieldChange(e,'description')}} placeholder="Enter Phone Number"></textarea>
			</div>
			<div className="form-group">
			<label>how to apply</label>
			<textarea  name="how_to_apply" className="form-control" value={this.state.jobData.how_to_apply } onChange={(e)=>{this.onFieldChange(e,'how_to_apply')}} placeholder="Enter Phone Number"></textarea>
			</div>
			<div className="form-group">
			<input type="submit" className="btn btn-success" value="Update"/> 
			</div>
			</form>
			</div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		user:state.catRd.EditEMProfile
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(NewJob);

