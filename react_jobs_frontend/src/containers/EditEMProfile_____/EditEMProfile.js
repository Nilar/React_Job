import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { getProfile , updateProfile} from '../../actions/employerActions';

class EditEMProfile extends Component {

constructor(){
  super();
  this.state = {
      userData:{
        name:'',
        phone:'',
        company_name:'',
        company_address:'',
        email:'',
        password:'',
        confirm_password:''
    }
  }
}

componentWillMount(){
this.props.getProfile();
        if(this.props.user){
        let {userData} = this.state;
            userData.name = this.props.user.name;
            userData.phone = this.props.user.phone;
            userData.email = this.props.user.email;
            userData.company_name = this.props.user.Employer.company_name;
            userData.company_address = this.props.user.Employer.company_address;
            this.setState({userData:userData});
        }
}

componentWillReceiveProps(newProps){

}


onFieldChange(e, field){
    let {userData} = this.state;
    
    switch (field){
        case "name":
        userData.name = e.target.value;
        this.setState({userData:userData});
        break;
        case "phone":
        userData.phone = e.target.value;
        this.setState({userData:userData});
        break;
        case "company_name":
        userData.company_name = e.target.value;
        this.setState({userData:userData});
        break;
        case "company_address":
        userData.company_address = e.target.value;
        this.setState({userData:userData});
        break;

        case "password":
        userData.password = e.target.value;
        this.setState({userData:userData});
        break;
        case "confirm_password":
        userData.confirm_password = e.target.value;
        this.setState({userData:userData});
        break;
    }
 }

 onFormSubmit(e){
     e.preventDefault();
     //todo validation 
  console.log(this.state.userData);
   this.props.updateProfile(this.state.userData);
 }

render() {

    return (
    <div className="container">
        <h3>Update your Profile dfd</h3>
        <form action="" method="POST" onSubmit={this.onFormSubmit.bind(this)}>
                    <div className="form-group">
                        <input type="text" className="form-control" id="name" value={this.state.userData.name} onChange={(e) => this.onFieldChange(e,"name")}   placeholder="Name" />
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" id="phone" value={this.state.userData.phone} onChange={(e) => this.onFieldChange(e,"phone")}  placeholder="Phone" />
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" id="company_name" value={this.state.userData.company_name} onChange={(e) => this.onFieldChange(e,"company_name")}   placeholder="Company Name" />
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" id="company_address" value={this.state.userData.company_address} onChange={(e) => this.onFieldChange(e,"company_address")}   placeholder="Company Address" />
                    </div>
                    <div className="form-group">
                        <input type="email" className="form-control" value={this.state.userData.email}   placeholder="Email" />
                    </div>
                    <div className="form-group">
                        <input type="password" className="form-control" onChange={(e) => this.onFieldChange(e,"password")}  placeholder="Password" />
                    </div>
                    <div className="form-group">
                        <input type="password" className="form-control" onChange={(e) => this.onFieldChange(e,"confirm_password")}  placeholder="Confirm Password" />
                    </div>
                    <button type="submit" className="btn btn-primary">Update</button>
        </form>

    </div>
    );
  }
}

function mapStateToProps(state){
	return {
    ...state,
    user: state.employerRd.currentUser,
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
         getProfile:getProfile,
         updateProfile:updateProfile   
	},dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EditEMProfile);
//export default EditEMProfile;
