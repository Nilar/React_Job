import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {Link} from 'react-router-dom';

class MyJobs extends Component {

	constructor(){
		super();
		this.state = {
			tab : ''
		}
	}

 

	  componentWillReceiveProps(newProps){
    if(newProps.tab != this.state.tab){
      this.setState({ tab : newProps.tab});
    }
  }
  
  render() {
  	let t = this.state.tab;
    return (
        <div className="container">
	        <h3>My Jobs</h3>
	    </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		user:state.catRd.MyJobs
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(MyJobs);

