import React, { Component } from 'react';
import './JobListing.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getJobs } from '../../actions/jobActions';
import ReactPaginate from 'react-paginate';
var moment = require('moment');

class JobListing extends Component {

	constructor(props){
		super(props);
		this.state ={
			query:{
				keyword:'',
				category:'',
				company:'',
				salary:'',
				page : 1
			}
		};
		this.handlePageClick = this.handlePageClick.bind(this);

	}


   componentWillMount(){
   	let query = this.props.query || {};
  	this.props.getJobs(query);
  }

  componentWillReceiveProps(nextProps){
		if(nextProps.query != this.state.query){
			this.setState({query:nextProps.query});
			this.props.getJobs(nextProps.query);
		}
	}

	handlePageClick(e){ 
		let page = e.selected + 1;
		let {query} = this.state;
		 query.page = page;
		this.setState({query : page}); //console.log(this.state.query);
		this.props.getJobs(this.state.query);
	}


  render() {
  	let jobsHtml = '';
	  if(!this.props.jobs){
	  	jobsHtml = <tr><td colSpan="5">Loading...</td></tr>;
	  }else if(this.props.jobs.length ==0){
	  	jobsHtml =<tr><td colSpan="5">No Jobs Found</td></tr>;
	  }else{
	  	jobsHtml = this.props.jobs.map((job,i)=>{
	  		let mt = moment(job.created_at).fromNow();
	  		return 	<tr key={job.id}>
						<td className="col-md-*">{job.title}</td>
						<td >{job.salary}</td>
						<td>{job.company}</td>
						<td>{job.description}</td>
						<td>{mt}</td>
	  				</tr>
	  	})
	  }
    return (
    <div className="table-responsive">
        <table className="table table-striped">
	       <tbody>
	       { jobsHtml }
	       </tbody> 	
	    </table>
	    <ReactPaginate previousLabel={"previous"}
                       nextLabel={"next"}
                       pageCount={this.props.total_pages}
                       marginPagesDisplayed={5}
                       pageRangeDisplayed={7}
                       onPageChange={this.handlePageClick}
                       containerClassName={"pagination justify-content-center"}
                       subContainerClassName={"pages pagination"}
                       pageClassName = {"page-item"}
                       pageLinkClassName = {"page-link"}
                       nextClassName ={"page-item"}
                       nextLinkClassName = {"page-link"}
                       previousClassName={"page-item"}
                       previousLinkClassName = {"page-link"}

						activeClassName={"active"} />
	</div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		jobs:state.jobRd.jobs,
		total_pages : state.jobRd.total_job_pages
	}

}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
		getJobs:getJobs,
	},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(JobListing);
