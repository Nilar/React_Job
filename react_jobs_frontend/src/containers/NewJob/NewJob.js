import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { submitJob } from '../../actions/jobActions';
import { getCategories } from '../../actions/categoryActions';

class NewJob extends Component {

constructor(){
  super();
  this.state = {
      tab: "",
      jobData: {
         title:'',
         salary:'',
         description:'',
         how_to_apply:'',
         category_id:''
      }
  }
}

componentWillMount(){
  this.props.getCategories();
}

componentWillReceiveProps(newProps){
    if(newProps.tab != this.state.tab){
    this.setState({tab:newProps.tab});
    }
}


onFieldChange(e, field){
    let {jobData} = this.state;
    
    switch (field){
        case "title":
        jobData.title = e.target.value;
        this.setState({jobData:jobData});
        break;

        case "salary":
        jobData.salary = e.target.value;
        this.setState({jobData:jobData});
        break;

        case "description":
        jobData.description = e.target.value;
        this.setState({jobData:jobData});
        break;

        case "how_to_apply":
        jobData.how_to_apply = e.target.value;
        this.setState({jobData:jobData});
        break;

        case "category_id":
        jobData.category_id = e.target.value;
        this.setState({jobData:jobData});
        break;

    }
 }

 onFormSubmit(e){
     e.preventDefault();
     //todo validation 
    
    this.props.submitJob(this.state.jobData);
 }

render() {
    let capOptions = this.props.categories.map((cat,i)=>{
      return <option key={cat.id} value={cat.id}>{cat.name}</option>
    });
    return (
    <div className="container">
    <h3 className="pt-4">Submit your job offer</h3>

        <form action="" method="POST" onSubmit={this.onFormSubmit.bind(this)}>
                    <div className="form-group">
                        <select className="form-control" defaultValue={this.state.jobData.category_id} onChange={(e) => this.onFieldChange(e,"category_id")} >
                          <option value="">Choose Category</option>
                          { capOptions }
                        </select>
                    </div>

                    <div className="form-group">
                        <input type="text" className="form-control" id="title" value={this.state.jobData.title} onChange={(e) => this.onFieldChange(e,"title")}   placeholder="Title" />
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" id="salary" value={this.state.jobData.salary} onChange={(e) => this.onFieldChange(e,"salary")}  placeholder="Salary" />
                    </div>
                    <div className="form-group">
                        <textarea className="form-control" id="description" value={this.state.jobData.description} onChange={(e) => this.onFieldChange(e,"description")}   placeholder="Job Description"></textarea>
                    </div>
                    <div className="form-group">
                        <textarea className="form-control" id="how_to_apply" value={this.state.jobData.how_to_apply} onChange={(e) => this.onFieldChange(e,"how_to_apply")}   placeholder="How to Apply" ></textarea>
                    </div>
                   
                    <button type="submit" className="btn btn-primary">Post</button>
        </form>
        
    </div>
    );
  }
}

function mapStateToProps(state){
	return {
    ...state,
    categories : state.catRd.categories || []
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
        submitJob: submitJob,
        getCategories : getCategories
	},dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NewJob);
//export default NewJob;
