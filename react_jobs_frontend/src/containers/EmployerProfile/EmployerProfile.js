import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';



import {Link} from 'react-router-dom';

import  EditEMProfile  from '../../containers/EditEMProfile/EditEMProfile';
import  MyJobs  from '../../containers/MyJobs/MyJobs';
import  NewJob  from '../../containers/NewJob/NewJob';

class EmployerProfile extends Component {

	constructor(){
		super();
		this.state = {
			tab : ""
		}
	}

  componentWillMount(){
  	this.setState({tab:this.props.tab});
  }

   componentWillReceiveProps(newProps){
    if(newProps.tab != this.state.tab){
      this.setState({ tab : newProps.tab});
    }
  }
 

  render() {
  	let t = this.state.tab; 

  	let tabContent = '';
  	if(t == "edit"){
  		tabContent = <EditEMProfile/>;
  	}else if(t == "jobs"){
  		tabContent = <MyJobs/>;
  	}else if(t == "newjob"){
  		tabContent = <NewJob/>;
  	}

    return (
    	<div className="container">
	        <div className="row">
		        <div className="col-12">
		        	<ul className="nav nav-tabs">
				      <li className="nav-item">
				         <Link className={ t == "edit"  ? "nav-link active" : "nav-link" } to="/profile/edit">Edit Profile</Link>
				      </li>
				      <li className="nav-item">
				        <Link className={ t == "jobs"  ? "nav-link active" : "nav-link" } to="/profile/jobs">My Job Posts</Link>
				      </li>
				      <li className="nav-item">
				        <Link className={ t == "newjob"  ? "nav-link active" : "nav-link" } to="/profile/newjob">Post a Job</Link>
				      </li>
				    </ul>
		        </div>  
		    </div>
		    <div className="row">
		        <div className="col-12">
		         { tabContent }
		        </div>
		    </div>
		 </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		user:state.catRd.EmployerProfile
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(EmployerProfile);

