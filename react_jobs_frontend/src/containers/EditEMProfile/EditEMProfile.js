import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { getProfile } from '../../actions/employerActions';
import { updateProfile } from '../../actions/employerActions';
import { updateProfilePhoto } from '../../actions/employerActions';

import {Link} from 'react-router-dom';
import $ from "jquery";
class EditEMProfile extends Component {

	constructor(){
		super();
		this.onFormSubmit = this.onFormSubmit.bind(this);

		this.state = {
			userData:{
				name:'',
				phone:'',
				company_name:'',
				company_address:'',
				email:'',
				password:'',
				confirm_password:'',
				company_logo: ''
			}
		}
	}

	componentWillMount(){
		this.props.getProfile();
		if(this.props.user){
			let { userData } = this.state;
			userData.name = this.props.user.name;
			userData.phone = this.props.user.phone;
			userData.email = this.props.user.email;
			userData.company_name = this.props.user.Employer.company_name;
			userData.company_address = this.props.user.Employer.company_address;
			userData.company_logo = this.props.user.Employer.company_logo;
			this.setState({userData : userData});
		}
	}

	componentWillReceiveProps(newProps){
		if(newProps.user != this.props.user){ 
			let { userData } = this.state;
			userData.company_logo = newProps.user.Employer.company_logo;
			this.setState({userData : userData});
		}
	}

	onFormSubmit(e){
		e.preventDefault();
		this.props.updateProfile(this.state.userData);
		$('#hideMe').fadeOut(5000);
	}

	onFieldChange(e,field){
		let {userData} = this.state;
		
		switch(field){
			case "name":
			userData.name = e.target.value;
			this.setState({userData:userData});
			break;
			case "phone":
			userData.phone = e.target.value;
			this.setState({userData:userData});
			break;
			case "company_name":
			userData.company_name = e.target.value;
			this.setState({userData:userData});
			break;
			case "company_address":
			userData.company_address = e.target.value;
			this.setState({userData:userData});
			break;
			case "email":
			userData.email = e.target.value;
			this.setState({userData:userData});
			break;
			case "password":
			userData.password = e.target.value;
			this.setState({userData:userData});
			break;
			case "confirm_password":
			userData.confirm_password = e.target.value;
			this.setState({userData:userData});
			break;
		}
	}
	onFileChoose(e){
		e.preventDefault();
		let files = e.target.files || e.dataTransfer.files;

		if(!files.length){
				console.log("nofile");
		}

		if(files[0]){
			this.props.updateProfilePhoto(files[0]);
		}

		

	}
	render() {
		let t = this.state.tab;
		let logo = (this.state.userData.company_logo) ? this.state.userData.company_logo  : "https://www.cre8ivedance.co.uk/secure/wp-content/uploads/2017/12/placeholder-square.jpg" 
		let msg = (this.props.success) ? this.props.success : ""
		return (
			<div className="container">
			<h3 className="pt-4">Update Your Profile</h3>
			
			<div className="row">
				<label className="mx-auto pt-3">
					<img src={logo} style={{'width':'150px'}} alt="companylogo" className="img-thumbnail"/>
					<input type="file" name="photo" onChange={this.onFileChoose.bind(this)} style={{"display": "none"}}/>
				</label>
			</div>
			<div className="text-success py-2" id="hideMe">{this.props.success}</div>
			<form onSubmit = {this.onFormSubmit.bind(this)}>
			<div className="form-group">
				<input type="text" name="name" className="form-control" value={this.state.userData.name } onChange={(e)=>{this.onFieldChange(e,'name')}}  placeholder="Enter Name"/>
			</div>
			<div className="form-group">
				<input type="email" name="email" className="form-control" value={this.state.userData.email } onChange={(e)=>{this.onFieldChange(e,'email')}}  placeholder="Enter Email"/>
			</div>
			<div className="form-group">
				<input type="text" name="phone" className="form-control" value={this.state.userData.phone } onChange={(e)=>{this.onFieldChange(e,'phone')}} placeholder="Enter Phone Number"/>
			</div>
			<div className="form-group">
				<input type="text" className="form-control" name="company_name" value={this.state.userData.company_name } onChange={(e)=>{this.onFieldChange(e,'company_name')}}  placeholder="Enter Company Name"/>
			</div>
			<div className="form-group">
				<input type="text" className="form-control" name="company_address" value={this.state.userData.company_address }  onChange={(e)=>{this.onFieldChange(e,'company_address')}} placeholder="Enter Company Address"/>
			</div>
			<div className="form-group">
				<input type="password" className="form-control" name="password" value={this.state.userData.password } onChange={(e)=>{this.onFieldChange(e,'password')}}  placeholder="your password"/>
			</div>
			<div className="form-group">
				<input type="password" className="form-control" name="confirm_password" value={this.state.userData.confirm_password } onChange={(e)=>{this.onFieldChange(e,'confirm_password')}}  placeholder="your confirm password"/>
			</div>
			<div className="form-group">
				<input type="submit" className="btn btn-success" value="Update"/> 
			</div>
			</form>
			</div>
			);
		}

	}

	function mapStateToProps(state){
		return{
			...state,
			user : state.employerRd.currentUser,
			success : state.employerRd.update
		}

	}

	function mapDispatchToProps(dispatch){
		return bindActionCreators({
			getProfile : getProfile,
			updateProfile : updateProfile,
			updateProfilePhoto : updateProfilePhoto
		},dispatch)

	}

	export default connect(mapStateToProps,mapDispatchToProps)(EditEMProfile);

