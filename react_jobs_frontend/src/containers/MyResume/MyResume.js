import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import { getMyResume } from '../../actions/resumeActions';
import ReactPaginate from 'react-paginate';
var moment = require('moment');

class MyResume extends Component {

constructor(){
  super();
  this.state = {
      tab: "",
      query:{
        page : 1
      }
  }
  /*this.handlePageClick = this.handlePageClick.bind(this);*/
}

componentWillMount(){
  this.props.getMyResume(this.state.query);
}

componentWillReceiveProps(newProps){
    if(newProps.tab != this.state.tab){
    this.setState({tab:newProps.tab});
    }
}

/*handlePageClick(e){ 
    let page = e.selected + 1;
    let {query} = this.state;
     query.page = page;
    this.setState({query : page});console.log(this.state.query);
    this.props.getJobs(this.state.query);
  }*/

render() {

   let resumesHTML = '';
    if(!this.props.resumes){
      resumesHTML = <tr><td colSpan="4">Loading...</td></tr>;
    }else if(this.props.resumes.length ==0){
      resumesHTML =<tr><td colSpan="4">No Resume Found</td></tr>;
    }else{
      resumesHTML = this.props.resumes.map((resume,i)=>{
        let mt = moment(resume.created_at).fromNow();
        return  <tr key={resume.id}>
            <td>{resume.title}</td>
             <td>{resume.salary}</td>
             <td>{resume.company}</td>
             <td>{resume.description}</td>
             <td>{mt}</td>
             </tr>
      })
    }

    return (
    <div className="container">
        <h3 className="pt-4">My Resume Posts</h3>
        <div>
           <table className="table table-striped">
           <tbody>
             {this.props.total_pages}
           { resumesHTML }
           </tbody>   
        </table>
         <ReactPaginate previousLabel={"previous"}
                       nextLabel={"next"}
                       pageCount={this.props.total_pages}
                       marginPagesDisplayed={5}
                       pageRangeDisplayed={7}
                       onPageChange={this.handlePageClick}
                       containerClassName={"pagination justify-content-center"}
                       subContainerClassName={"pages pagination"}
                       pageClassName = {"page-item"}
                       pageLinkClassName = {"page-link"}
                       nextClassName ={"page-item"}
                       nextLinkClassName = {"page-link"}
                       previousClassName={"page-item"}
                       previousLinkClassName = {"page-link"}

            activeClassName={"active"} />
        </div>
        
    </div>
    );
  }
}

function mapStateToProps(state){
	return {
    ...state,
    resumes:state.resumeRd.resumes,
    total_pages : state.resumeRd.total_resume_pages
  
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
    getMyResume:getMyResume  
	},dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MyResume);
//export default MyJobs;
