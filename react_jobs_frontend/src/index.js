import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route , Switch,
    Redirect } from "react-router-dom";
import registerServiceWorker from './registerServiceWorker';
import { store , history } from './store';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import './index.css';
import App from './App';

ReactDOM.render((
	<Provider store={store}>
	    <ConnectedRouter history={history}>
	      <Switch>
	        <Route path="/" component={App} />
	      </Switch>
	    </ConnectedRouter>
    </Provider>
), document.getElementById('root'));
registerServiceWorker();
