import{
	GET_RESUMES,
	GET_RESUMES_OK,
	GET_RESUMES_ERR,
	POST_RESUME,
	POST_RESUME_OK,
	POST_RESUME_ERR,
	RESUME_PDF_UPDATE,
	RESUME_PDF_UPDATE_OK,
	RESUME_PDF_UPDATE_ERR,
	GET_MYRESUME,
	GET_MYRESUME_OK,
	GET_MYRESUME_ERR

} from '../config/constants';

const defaultState = {
	inProgress:false,
	resumes:null,
	total_resume_pages : 0,
	savemsg : null
};

export default (state=defaultState , action)=>{
	switch(action.type){
		case GET_RESUMES:
		  return{
		  	...state,
		  	inProgress:true
		  };
		 

		case GET_RESUMES_OK:
			return{
				...state,
				inProgress:false,
				resumes:action.payload.resumes || [],
				total_resume_pages : action.payload.total_pages || 0
			};
		  

		case GET_RESUMES_ERR:
			return{
				...state,
				inProgress:false
			};
		
		case POST_RESUME:
		return{
			...state,
			inProgress:true
		};

		case POST_RESUME_OK:
		return{
			...state,
			inProgress:false,
			resumes : action.payload.resume,
			savemsg: "Save Resume Successfully!"
		};
		
		case POST_RESUME_ERR:
			return{
				...state,
				inProgress:false
			};

		case RESUME_PDF_UPDATE:
		return{
			...state,
			inProgress:true
		};

		case RESUME_PDF_UPDATE_OK:
		return{
			...state,
			inProgress:false,
		//	resumes : action.payload.resume,
			//savemsg: "Save Resume Successfully!"
		};
		
		case RESUME_PDF_UPDATE_ERR:
			return{
				...state,
				inProgress:false
			};

		case GET_MYRESUME:
		return{
			...state,
			inProgress:true
		};

		case GET_MYRESUME_OK:
		console.log(action.payload.resumes)
		return{
			...state,
			inProgress:false,
			resumes : action.payload.resumes || [],
			total_resume_pages : action.payload.total_pages || 0
		};
		
		case GET_MYRESUME_ERR:
			return{
				...state,
				inProgress:false
			};

		default:
			return state;
	}
}