import{
	GET_EMPLOYERS,
	GET_EMPLOYER_OK,
	GET_EMPLOYERS_OK,
	GET_EMPLOYERS_ERR,
	EMPLOYER_LOGIN,
	EMPLOYER_LOGIN_OK,
	EMPLOYER_LOGIN_ERR,
	EMPLOYER_REGISTER,
	EMPLOYER_REGISTER_OK,
	EMPLOYER_REGISTER_ERR,
	EMPLOYER_UPDATE,
	EMPLOYER_UPDATE_OK,
	EMPLOYER_UPDATE_ERR,
	LOGOUT
} from '../config/constants';

let u = localStorage.getItem("_user") ? JSON.parse(localStorage.getItem("_user")) : null;


const defaultState = {
	inProgress:false,
	loginProgress:false,
	employers:null,
	employer:null,
	currentUser:u,
	token:localStorage.getItem("_token"),
	update : null,
	respondesc : ""
};

export default (state=defaultState , action)=>{
	switch(action.type){
		case GET_EMPLOYERS:
			return{
				...state,
				inProgress:true
			};
		// Register
		case EMPLOYER_REGISTER:
			return {
				...state,
				inProgress : true
			}

		case EMPLOYER_REGISTER_OK:
			return {
				...state,
				inProgress : false
			}

		case EMPLOYER_REGISTER_ERR:
			return {
				...state,
				inProgress : false
			}

		case GET_EMPLOYERS_OK:
		//console.log(action.payload)
			return{
				...state,
				inProgress:false,
				employers : action.payload.employers || []
			};

		case GET_EMPLOYER_OK:
			return{
				...state,
				inProgress:false,
				currentUser : action.payload.user || {}
			};

		// case GET_PROFILE_OK:
		// return{
		// 	...state,
		// 	inProgress:false,
		// 	currentUser : action.payload.user || {}
		// };


		case GET_EMPLOYERS_ERR:
			return{
				...state,
				inProgress:false
			};
		// Login
		case EMPLOYER_LOGIN:
			return{
				...state,
				loginProgress:true
			};

		case EMPLOYER_LOGIN_OK:
			return{
				...state,
				loginProgress:false,
				currentUser:action.payload.user ,
				token:action.payload.token
			};

		case EMPLOYER_LOGIN_ERR:
			return{
				...state,
				loginProgress:false,
				respondesc:action.payload.response.data.message
			};
		// Update
		case EMPLOYER_UPDATE:
			return{
				...state
			};
		case EMPLOYER_UPDATE_OK:
		return{
			...state,
			update: "Update Successfully!"
		};

		case EMPLOYER_UPDATE_ERR:
		return{
			...state,
		};

		case LOGOUT:
			return{
				...state,
				currentUser:null,
				token:null
			};

		default:
		return state;
	}
}