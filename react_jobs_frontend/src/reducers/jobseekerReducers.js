import{
	JOBSEEKER_REGISTER,
	JOBSEEKER_REGISTER_OK,
	JOBSEEKER_REGISTER_ERR,
	JOBSEEKER_LOGIN,
	JOBSEEKER_LOGIN_OK,
	JOBSEEKER_LOGIN_ERR,
	JOBSEEKER_UPDATE,
	JOBSEEKER_UPDATE_OK,
	JOBSEEKER_UPDATE_ERR
} from '../config/constants';


let u = localStorage.getItem("_user")?JSON.parse(localStorage.getItem("_user")):null;
const defaultState = {
	inProgress:false,
	loginProgress:false,
	token:localStorage.getItem("_token"),
	update : null,
	respondesc : ""
};

export default (state=defaultState , action)=>{
	switch(action.type){
		// Register
		case JOBSEEKER_REGISTER:
		  return{
		  	...state,
		  	inProgress:true
		  };
		  break;

		case JOBSEEKER_REGISTER_OK:
			return{
				...state,
				inProgress:false,
				jobseekerregister:action.payload.jobseekerregister || []
			};
		  break;

		case JOBSEEKER_REGISTER_ERR:
			return{
				...state,
				inProgress:false
			};
		  break;
			// Login
		case JOBSEEKER_LOGIN:
		  return{
		  	...state,
		  	inProgress:true
		  };
		  break;

		case JOBSEEKER_LOGIN_OK:
	
			return{
				...state,
				inProgress:false,
				jobseekerlogin:action.payload.jobseekerlogin || []
			};
			break;
		case JOBSEEKER_LOGIN_ERR:
		
			return{
				...state,
				inProgress:false,
				respondesc:action.payload.response.data.message
			};
			break;
		//Update
		case JOBSEEKER_UPDATE:
			return{
				...state,
				inProgress:true
			};
		case JOBSEEKER_UPDATE_OK:
		return{
			...state,
			inProgress:false,
			update: "Update Successfully!"
		};

		case JOBSEEKER_UPDATE_ERR:
		return{
			...state,
			inProgress:false
		};
		

		default:
			return state;
	}
}