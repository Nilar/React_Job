import{
	APP_LOAD,
	REDIRECT,
	WELCOME
} from '../config/constants';

const defaultState = {
	appName: 'React Jobs',
	errors: null,
	inProgress:false,
	message:null
}

export default (state=defaultState , action)=>{
	switch(action.type){
		case APP_LOAD:
		  return{
		  	...state,
		  	errors:action.error?action.payload.errors:null,
		  };
		  break;

		case REDIRECT:
			return{...state,redirectTo:null};
		  break;

		case WELCOME:
			return{...state,message:"Hi"};
		  break;

		default:
			return state;
	}
}