import React, { Component } from 'react';
import './ForJobSeeker.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { jobseekerLogin,jobseekerRegister } from '../../actions/jobseekerActions';
import $ from "jquery";

var toastr = require('toastr'); 
toastr.options={
	positionClass : 'toast-bottom-left',
	showEasing : "swing"
}
class ForJobSeeker extends Component {

	constructor(){
		super();
		this.onLoginFormSubmit = this.onLoginFormSubmit.bind(this);
		this.onRegisterFormSubmit = this.onRegisterFormSubmit.bind(this);

		this.state = {
			loginData:{
				email:'',
				password:''
			},
			registerData:{
				name:'',
				phone:'',
				career_title:'',
				gender:'',
				birthday : '',
				password:'',
				confirm_password:''
			},
			Validation_err:{
				name:'',
				phone:'',
				career_title:'',
				gender:'',
				birthday : ''
			}
		}
	}

	onLoginFormSubmit(e){
		e.preventDefault();
		let {loginData} = this.state;
		
		if(!loginData.email || !loginData.password){
			toastr.warning("Please Type Phone No and Password");
			return;
		}
		this.props.login(loginData);
		$('#hideMe').fadeOut(5000);

	}

	onRegisterFormSubmit(e){
		e.preventDefault();
		let {registerData,Validation_err} = this.state;

		Validation_err.career_title = "";
		Validation_err.gender = "";
		Validation_err.birthday = "";
		Validation_err.name = "";
		Validation_err.phone = "";
		
		if(registerData.career_title.length < 1){
			Validation_err.career_title = "Please fill Career Title";
		}
		
		else if(registerData.gender.length <1){
			Validation_err.gender = "Please Choose Gender";
		}
		else if(registerData.birthday.length < 1){
			Validation_err.birthday = "Please fill Birthday";
		}
		else if(registerData.name.length < 1){
			Validation_err.name = "Please fill Name";
		}
		else if(registerData.phone.length < 9){
			Validation_err.phone = "Please type valid Phone number";
		}
		else if(registerData.password.length<3 && registerData.password.length >=10){
			Validation_err.password =  "Please fill password between 3 to 10.";
		}
		else if(registerData.password != registerData.confirm_password){
			Validation_err.confirm_password =  "Please type same password";
		}
		
		if(Validation_err.career_title != "" || Validation_err.gender != "" || Validation_err.birthday != "" || Validation_err.phone != ""
		|| Validation_err.name != "" ){
			this.setState({Validation_err: Validation_err});
			toastr.warning("Please review your form submission again");
			return;
		}
		this.props.register(registerData);
	}

	onLoginFieldChange(e,field){
		let {loginData} = this.state;
		switch(field){
			case "email":
			loginData.email = e.target.value;
			this.setState({loginData:loginData});
			break;
			case "password":
			loginData.password = e.target.value;
			this.setState({loginData:loginData});
			break;
		}
	}

	onRegisterFieldChange(e,field){
		let {registerData} = this.state;
		switch(field){
			case "name":
			registerData.name = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "phone":
			registerData.phone = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "career_title":
			registerData.career_title = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "gender":
			registerData.gender = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "birthday":
			registerData.birthday = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "password":
			registerData.password = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "confirm_password":
			registerData.confirm_password = e.target.value;
			this.setState({registerData:registerData});
			break;
		}
	}

   render() {
	   let regtext = this.props.inProgress ? "Processing" : "Register";
	   let logintext = this.props.loginProgress ? "Processing" : "login";
    return (
        <div className="container foremployer">
        	<div className="row pt-4">
           		<h1>JobSeeker find your dream jobs</h1>
           	</div>
        	<div className="row">
        		<div className="col-md-6">
        			<div className="container"> 
        				<form action="" method="post" onSubmit={this.onLoginFormSubmit}>
        				  <h4> Login </h4>
						  <div className="text-success py-2" id="hideMe">{this.props.respondesc}</div>
						  <div className="form-group">
						    <input type="text" name="email" className="form-control" onChange={(e)=>{this.onLoginFieldChange(e,'email')}}  placeholder="959xxxxxxxxx"/>
						  </div>
						  <div className="form-group">
						    <input type="password" name="password" className="form-control" onChange={(e)=>{this.onLoginFieldChange(e,'password')}}  placeholder="Password"/>
						  </div>
						  <div className="form-group">
						  	<input type="submit" className="btn btn-success" value={logintext}/>
						  </div>
						</form>
        			</div>
        		</div>
        		<div className="col-md-6">
        		     <div className="container"> 
        				<form onSubmit = {this.onRegisterFormSubmit}>
        				  <h4> Register </h4>
        				   <div className="form-group">
						    <input type="text" name="career_title" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'career_title')}} placeholder="Career"/>
						  	<span className="error">{this.state.Validation_err.career_title}</span>

						  </div>
						   <div className="form-group">
						    <label className="form-check-inline">
						    	<input type="radio" className="form-check-input" onChange={(e)=>{this.onRegisterFieldChange(e,'gender')}}  name="gender" value="1"/>Male
							</label>
							<label className="form-check-inline">
						    	<input type="radio" className="form-check-input" onChange={(e)=>{this.onRegisterFieldChange(e,'gender')}}  name="gender" value="0"/>Female
							</label>
							<span className="error">{this.state.Validation_err.gender}</span>
						  </div>
						<div className="form-group">
						    <input type="date" name="birthday" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'birthday')}} placeholder="Enter birthday"/>
							<span className="error">{this.state.Validation_err.birthday}</span>
						  </div>
        				   <div className="form-group">
						    <input type="text" name="name" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'name')}}  placeholder="Name"/>
							<span className="error">{this.state.Validation_err.name}</span>
						  </div>
						   <div className="form-group">
						    <input type="text" name="phone" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'phone')}} placeholder="Phone Number"/>
							<span className="error">{this.state.Validation_err.phone}</span>
						  </div>
						   <div className="form-group">
						    <input type="password" className="form-control" name="password" onChange={(e)=>{this.onRegisterFieldChange(e,'password')}}  placeholder="Password"/>
							<span className="error">{this.state.Validation_err.password}</span>
						  </div>
						   <div className="form-group">
						    <input type="password" className="form-control" name="confirm_password" onChange={(e)=>{this.onRegisterFieldChange(e,'confirm_password')}}  placeholder="Confirm Password"/>
							<span className="error">{this.state.Validation_err.confirm_password}</span>

						  </div>
						  <div className="form-group">
						  	<input type="submit" className="btn btn-success" value={regtext}/>
						  </div>
						</form>
        			</div>
        		</div>
        	</div>
	     </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		inProgress : state.jobseekerRd.inProgress,
		loginProgress : state.jobseekerRd.loginProgress,
		respondesc : state.jobseekerRd.respondesc
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		login:jobseekerLogin,
		register:jobseekerRegister
	},dispatch)
	
}


export default connect(mapStateToProps,mapDispatchToProps)(ForJobSeeker);
