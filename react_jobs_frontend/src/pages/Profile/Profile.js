import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { employerLogin, employerRegister } from '../../actions/employerActions'

import  JobSeekerProfile  from '../../containers/JobSeekerProfile/JobSeekerProfile';
import  EmployerProfile from '../../containers/EmployerProfile/EmployerProfile';

class Profile extends Component {

	constructor(){
		super();
    this.state = {
      tab : "edit"
    }
	}

  componentWillMount(props){
     let tab = (this.props.match.params.tab) ? this.props.match.params.tab : "edit"; 
     this.setState({tab : tab});
  }

  componentWillReceiveProps(newProps){
    if(newProps.match.params.tab != this.state.tab && newProps.match.params.tab != null){
      this.setState({ tab : newProps.match.params.tab});
    }
  }

  render() { 
   	let content = (this.props.user.role == 0) ? <JobSeekerProfile tab={this.state.tab}/> : <EmployerProfile tab={this.state.tab}/>;
    return (
        <div className="container foremployer">
        	 <div className="row ml-2 py-3">
             <h4>My Profile!</h4>
           </div>
        	<div className="row ml-2">
            <h6 className="">Welcome , { this.props.user.name }</h6>
        	  { content }
        	</div>
	     </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		user:state.employerRd.currentUser || {}
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
	},dispatch)
	
}

export default connect(mapStateToProps,mapDispatchToProps)(Profile);

