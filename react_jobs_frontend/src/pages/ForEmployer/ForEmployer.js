import React, { Component } from 'react';
import './ForEmployer.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { employerLogin,employerRegister } from '../../actions/employerActions';
import $ from "jquery";

var toastr = require('toastr'); 
toastr.options ={
	positionClass:'toast-bottom-left'
}
class ForEmployer extends Component {

	constructor(){
		super();
		this.onLoginFormSubmit = this.onLoginFormSubmit.bind(this);
		this.onRegisterFormSubmit = this.onRegisterFormSubmit.bind(this);

		this.state = {
			loginData:{
				email:'',
				password:''
			},
			registerData:{
				name:'',
				phone:'',
				company_name:'',
				company_address:'',
				email:'',
				password:'',
				confirm_password:''
			},
			Validation_err:{
				name : '',
				phone: '',
				email : '',
				company_name : '',
				company_address : ''
			}
		}
	}

	onLoginFormSubmit(e){
		e.preventDefault();
		let {loginData} = this.state;
		if(!loginData.email || !loginData.password){
			toastr.warning("Please Type Phone No and Password");
			return;
		}
		this.props.login(loginData);
		//$('#hideMe').fadeOut(5000);

	}
	isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
	}
	onRegisterFormSubmit(e){
		e.preventDefault();
		let {registerData,Validation_err} = this.state;

		Validation_err.name = "";
		Validation_err.email = "";
		Validation_err.phone = "";
		Validation_err.company_name = "";
		Validation_err.company_address = "";
		
		if(registerData.name.length<1){
				Validation_err.name =  "Please fill your name";
		}
		else if(!this.isEmail(registerData.email)){
			Validation_err.email =  "Please type valid email address";
		}
		else if(registerData.phone.length<9){
			Validation_err.phone =  "Please type valid phone number";
		}
		else if(registerData.company_name.length<1){
			Validation_err.company_name =  "Please fill company name";
		}
		else if(registerData.company_address.length<1){
			Validation_err.company_address =  "Please fill company address";
		}
		else if(registerData.password.length<3 && registerData.password.length >=10){
			Validation_err.password =  "Please fill password between 3 to 10.";
		}
		else if(registerData.password != registerData.confirm_password){
			Validation_err.confirm_password =  "Please type same password";
		}
		
		
		if(Validation_err.name != "" || Validation_err.email != "" || Validation_err.phone != "" || 
		Validation_err.company_name != "" || Validation_err.company_address != ""){
			this.setState({Validation_err: Validation_err});
			toastr.warning("Please review your form submission again.", "Validation errors!");
			return;
		}
		this.props.register(registerData);
	}

	onLoginFieldChange(e,field){
		let {loginData} = this.state;
		switch(field){
			case "email":
			loginData.email = e.target.value;
			this.setState({loginData:loginData});
			break;
			case "password":
			loginData.password = e.target.value;
			this.setState({loginData:loginData});
			break;
		}
	}

	onRegisterFieldChange(e,field){
		let {registerData} = this.state;
		switch(field){
			case "name":
			registerData.name = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "phone":
			registerData.phone = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "company_name":
			registerData.company_name = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "company_address":
			registerData.company_address = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "email":
			registerData.email = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "password":
			registerData.password = e.target.value;
			this.setState({registerData:registerData});
			break;
			case "confirm_password":
			registerData.confirm_password = e.target.value;
			this.setState({registerData:registerData});
			break;
		}
	}

  render() { 
  	let btnText = ( this.props.inProgress ) ? 'Processing..':'Register' ;
  	let loginText = ( this.props.loginProgress ) ? 'Processing..':'login' ;
    return (
        <div className="container foremployer">
        	 <div className="row pt-4">
             <h3>Employer find your candidate!</h3>
           </div>
        	<div className="row">
        		<div className="col-md-6">
        			<div className="container"> 
        				<form action="" method="post" onSubmit={this.onLoginFormSubmit}>
        				  <h4> Login </h4>
						  	{/* <div className="text-success py-2" id="hideMe">{this.props.respondesc}</div> */}

							<div className="form-group">
								<input type="text" name="email" className="form-control" onChange={(e)=>{this.onLoginFieldChange(e,'email')}}  placeholder="example@gmail.com"/>
							</div>
							<div className="form-group">
								<input type="password" name="password" className="form-control" onChange={(e)=>{this.onLoginFieldChange(e,'password')}}  placeholder="Password"/>
							</div>
							<div className="form-group">
								<input type="submit" className="btn btn-success" value={ loginText }/>
							</div>
						</form>
        			</div>
        		</div>
        		<div className="col-md-6">
        		     <div className="container"> 
        				<form onSubmit = {this.onRegisterFormSubmit}>
        				  <h4> Sign Up Now </h4>
        				   	<div className="form-group">
											<input type="text" name="name" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'name')}}  placeholder="Name"/>
											<span className="error">{this.state.Validation_err.name}</span>
										</div>
										<div className="form-group">
											<input type="email" name="email" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'email')}}  placeholder="example@gmail.com"/>
											<span className="error">{this.state.Validation_err.email}</span>
										</div>
										<div className="form-group">
											<input type="text" name="phone" className="form-control" onChange={(e)=>{this.onRegisterFieldChange(e,'phone')}} placeholder="Phone Number"/>
											<span className="error">{this.state.Validation_err.phone}</span>
										</div>
										<div className="form-group">
											<input type="text" className="form-control" name="company_name" onChange={(e)=>{this.onRegisterFieldChange(e,'company_name')}}  placeholder="Company Name"/>
											<span className="error">{this.state.Validation_err.company_name}</span>

										</div>
										<div className="form-group">
											<input type="text" className="form-control" name="company_address" onChange={(e)=>{this.onRegisterFieldChange(e,'company_address')}} placeholder="Company Address"/>
											<span className="error">{this.state.Validation_err.company_address}</span>
										</div>
										<div className="form-group">
											<input type="password" className="form-control" name="password" onChange={(e)=>{this.onRegisterFieldChange(e,'password')}}  placeholder="Password"/>
											<span className="error">{this.state.Validation_err.password}</span>
										</div>
										<div className="form-group">
											<input type="password" className="form-control" name="confirm_password" onChange={(e)=>{this.onRegisterFieldChange(e,'confirm_password')}}  placeholder="Confirm Password"/>
											<span className="error">{this.state.Validation_err.confirm_password}</span>
										</div>
										<div className="form-group">
											<input type="submit" className="btn btn-success" value={ btnText }/>
										</div>
						</form>
        			</div>
        		</div>
        	</div>
	     </div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		inProgress : state.employerRd.inProgress,
		loginProgress : state.employerRd.loginProgress,
		respondesc : state.employerRd.respondesc

	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		login:employerLogin,
		register:employerRegister
	},dispatch)
	
}

export default connect(mapStateToProps,mapDispatchToProps)(ForEmployer);

