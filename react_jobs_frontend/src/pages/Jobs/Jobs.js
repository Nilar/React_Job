import React, { Component } from 'react';
import './Jobs.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import JobListing from '../../containers/JobListing/JobListing';
import { getCategories } from '../../actions/categoryActions';
import { getEmployers } from '../../actions/employerActions';
import { getSalaries } from '../../actions/jobActions';

class Jobs extends Component {

	constructor(){
		super();
		this.state ={
			keyword:'',
			category:'',
			company:'',
			salary:''
		}

		this.updateSate = this.updateSate.bind(this);
		this.onFormSubmit = this.onFormSubmit.bind(this);
	}

	componentWillMount(){
		let c = this.props.match.params.category;
		if( c!= undefined){ //alert(c);
			this.setState({category:c});
		}

		let com = this.props.match.params.company;
		if(com != undefined){
			this.setState({company: com});
		}
		
		this.props.getCats();
	    this.props.getComs();
	    this.props.getSalaries();
	}

	updateSate (field , ev)  {
		const state = this.state;
		const newState = Object.assign({},state,{ [field]:ev.target.value});
		this.setState(newState); console.log(this.state);
	}	

	onFormSubmit(e){
		e.preventDefault();
		console.log(this.state);
	}

  render() {
  	let catsOptions ='';
  	if(this.props.categories){
  		catsOptions = this.props.categories.map((cat,i)=>{
  			return <option  key={i} value={cat.id}>{cat.name}</option>;
  		});
  	}

  	let companiesOptions ='';
  	if(this.props.companies){
  		companiesOptions = this.props.companies.map((com,i)=>{
  			return <option  key={i} value={com.id}>{com.company_name}</option>;
  		});
  	}



  	let salaryOptions ='';
  	if(this.props.salaries){
  		salaryOptions = this.props.salaries.map((sal,i)=>{
  			return <option key={i} value={i}>{sal.label}</option>
  		})
  	}

    return (
    	<div className="container-fluid">
	    		<form className="search-form row my-4" onSubmit = {this.onFormSubmit}>
	    			<div className="col col-md-3">
	    				<input type="text" name="keyword" onChange={(e)=>{this.updateSate('keyword',e)}} className="form-control" placeholder="Type keyword to search"/>
	    			</div>
	    			<div className="col col-md-2">
	    				<select className="form-control" name="category=" onChange={(e)=>{this.updateSate('category',e)}}>
	    				<option value="">Category</option>
	    				<option >Information Category</option>
	    				{ catsOptions }
	    				</select>
	    			</div>
	    			<div className="col col-md-2">
	    				<select className="form-control" name="company"  onChange={(e)=>{this.updateSate('company',e)}}>
	    				<option value="">Company</option>
	    				{ companiesOptions }
	    				</select>
	    			</div>
	    			<div className="col col-md-2" name="salary"  onChange={(e)=>{this.updateSate('salary',e)}}>
	    				<select className="form-control">
	    				<option value="">Salary</option>
	    				{ salaryOptions }
	    				</select>
	    			</div>
	    			{/*<div className="col col-md-2">
	    				<button className="btn btn-primary">Search</button>
	    			</div>*/}
	    		</form>
     	  <JobListing query={this.state} />
     	</div>
    );
  }

}

function mapStateToProps(state){
	return{
		...state,
		categories:state.catRd.categories,
		companies:state.employerRd.employers,
		salaries:state.jobRd.salaries
	}

}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		getCats:getCategories,
		getComs:getEmployers,
		getSalaries:getSalaries
	},dispatch)
  
}

export default connect(mapStateToProps,mapDispatchToProps)(Jobs);
