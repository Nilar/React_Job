import React, { Component } from 'react';
import './Home.css';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { sayHi } from '../../actions/commonActions';

import Categories from '../../containers/Categories/Categories';
import PopularCompanies from '../../containers/PopularCompanies/PopularCompanies';
import PopularJobs from '../../containers/PopularJobs/PopularJobs';
var accounting = require("accounting");
class Home extends Component {

constructor(){
	super();
	this.onBtnClick = this.onBtnClick.bind(this);
}


  onBtnClick(){
  	this.props.sayHi();
  }

  render() {
  	if(this.props.message){
  		alert(this.props.message);
  	}
    return (
        <div className="container-fluid">
         {/*  <button onClick={this.onBtnClick}>Click to Say Hi</button>  */} 
	        <Categories />
	        <PopularCompanies />
	        <PopularJobs />
			{/* <FormatMoney /> */}
        </div>
    );
  }

}

// export const FormatMoney = (amount=1000, precision = 2) => {
// 	if(!parseFloat(amount)) amount = 0;
// 	return accounting.formatMoney(amount, {
// 	  symbol: 'MMK',
// 		precision: precision,
// 		thousand: ',',
// 		format: {
// 			pos : '%v %s',
// 			neg : '(%v) %s',
// 			zero: '0 %s',
// 		}
// 	});
//    }


function mapStateToProps(state){
	return{
		...state,
		message:state.commonRd.message
	};
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
			sayHi:sayHi,
			
	},dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Home);
