var express = require('express');
var router = express.Router();
var models = require('../models');


// function show(res,data,status,msg){
//   return res.json();

// }
/* GET jobs listing. */
router.get('/', async function(req, res, next) {
    
    let jobs = await models.Job.findAll();
    res.json({
      jobs : jobs
    })
});

/* Get single jobs listing*/
router.get('/:id',async function(req,res,next){
  let id = req.params.id || 0;
  console.log(id);
  let job = await models.Job.findOne({where:{id:id}});
      
  return res.json({
    job : job
  })
})

/* post new job*/

router.post('/',async function(req,res,next) {
  let formData = req.body;
  if(formData){
    let job = await models.Job.create(formData);

    return res.json({
      status : 200,
      message : 'Success'
    })
  }else{
    res.json({
      status : 401,
      message : 'Validation error'
    })
  }
  
});

/* Put a job*/

router.put('/:id',async function(req,res,next) {
    let id = req.params.id;
    let formData =req.body;
    let ok = models.Job.update(formData,{
        where :{id :id}
    });

    res.json({
      status : 200,
      message : 'Update Success'
    })
});

/* Delete a job*/

router.delete('/:id',function(req,res,next) {
  let id = req.params.id;
   models.Job.destory({
     where :{id:id}
   });
   res.json({
    status : 200,
    message : 'Delete Success'
  })
});

module.exports = router;
