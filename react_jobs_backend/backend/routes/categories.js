var express = require('express');
var router = express.Router();
var models = require("../models");


/* GO List page. */
router.get('/',  async function(req, res, next) {
	let cats = await models.Category.findAll();
	res.render('category/index', { data: cats });
});

/* GO Create page. */
router.get('/create',async function(req, res, next) {

	res.render('category/create');
});

/* Store Category */
router.post('/', async function(req, res, next) {

	let formData = req.body;
	var errors = [];
	if(!formData.name){
		req.flash("errors","Name is required");
		errors.push("Name is required");
	}

	if(errors.length > 0){
		return res.redirect('/categories/create');
	}

	let category = await models.Category.create(formData);
	res.redirect('/categories');

});

/* @edit */
router.get('/edit/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let category = await models.Category.findOne({ where: {id: id} });console.log(category);
	res.render('category/edit', { data: category });
});

/* @update */
router.post('/update/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let formData = req.body;

	var errors = [];
	if(!id || !formData.name){
		req.flash("errors","Name is required");
		errors.push("Name is required");
	}

	if(errors.length > 0){
		return res.redirect('/categories/edit/'+id);
	}

	let category = await models.Category.update(formData,{where:{id:id}});

	res.redirect('/categories');
});

/* @delete */
router.post('/delete/:id', async function(req, res, next) {

	let id = req.params.id || 0;

	if(!id){
		return res.status(400).json({
			success : false,
			message : 'Validation error!'
		});
	}

	let category = await models.Category.destroy({where:{id:id}});
	res.redirect('/categories');
});

module.exports = router;
