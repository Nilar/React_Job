var express = require('express');
var router = express.Router();
var models = require("../models");

/* GO List page. */
router.get('/',async function(req, res, next) {
	
// ASYNC/AWAIT WAY
let employers =  await models.User.findAll({ where: {role: 2 , status:1} });
res.render('employers/index', { data: employers });
});
/* GO Create page. */
router.get('/create',async function(req, res, next) {

	res.render('employers/create');
});
/* Store Category */
router.post('/', async function(req, res, next) {

	let formData = req.body;
	var errors = [];

	if(!formData.name){
		req.flash("errors","Name is required");
		errors.push("Name is required");
	}

	if(errors.length > 0){
		return res.redirect('/employers/create');
	}
	let employers = await models.User.create(formData);
	res.redirect('/employers');

});
/* @edit */
router.get('/edit/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let employer = await models.User.findOne({ where: {id: id} });

	res.render('employers/edit', { employer: employer  });
});
/* @update */
router.post('/update/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let formData = req.body;

	var errors = [];
	if(!formData.name){
		req.flash("errors","Name is required");
		errors.push("Name is required");
	}

	if(errors.length > 0){
		return res.redirect('/employers/edit'+id);
	}

	let employer = await models.User.update(formData,{where:{id:id}});

	res.redirect('/employers');
});
/* @delete */
router.post('/delete/:id', async function(req, res, next) {

	let id = req.params.id || 0;

	let employer = await models.User.destroy({where:{id:id}});
	res.redirect('/employers');
});
module.exports = router;
