var express = require('express');
var router = express.Router();
var models = require("../models");

/* GO List page. */
router.get('/',async function(req, res, next) {
	
// ASYNC/AWAIT WAY
let jobseekers =  await models.User.findAll({ where: {role: 1 , status:1} });
res.render('jobseekers/index', { data: jobseekers });
});
/* GO Create page. */
router.get('/create',async function(req, res, next) {

	res.render('jobseekers/create');
});
/* Store Category */
router.post('/', async function(req, res, next) {

	let formData = req.body;
	var errors = [];

	if(!formData.name){
		req.flash("errors","Name is required");
		errors.push("Name is required");
	}

	if(errors.length > 0){
		return res.redirect('/jobseekers/create');
	}
	let jobseekers = await models.User.create(formData);
	res.redirect('/jobseekers');

});
/* @edit */
router.get('/edit/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let jobseeker = await models.User.findOne({ where: {id: id} });

	res.render('jobseekers/edit', { jobseeker: jobseeker  });
});
/* @update */
router.post('/update/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let formData = req.body;

	var errors = [];
	if(!formData.name){
		req.flash("errors","Name is required");
		errors.push("Name is required");
	}

	if(errors.length > 0){
		return res.redirect('/jobseekers/edit'+id);
	}

	let jobseeker = await models.User.update(formData,{where:{id:id}});

	res.redirect('/jobseekers');
});
/* @delete */
router.post('/delete/:id', async function(req, res, next) {

	let id = req.params.id || 0;

	let jobseeker = await models.User.destroy({where:{id:id}});
	res.redirect('/jobseekers');
});
module.exports = router;
