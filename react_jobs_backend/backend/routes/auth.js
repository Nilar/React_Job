var express = require('express');
var router = express.Router();
var passport = require('passport');
var bcrypt = require('bcrypt');

var models = require("../models");

/* GET login Form. */
router.get('/login', function(req, res, next) {
 res.render("auth/login");
});

/* Post Login Form */
router.post('/login',passport.authenticate('local', { failureRedirect: '/auth/login',failureFlash : true }), function(req, res, next) {
	
	res.redirect('/');
});
/* Get User Out */
router.get("/logout",function(req, res,next){
	req.logout();
	 res.redirect('/');
})
/* GET register Form. */
router.get('/register', function(req, res, next) {
 res.render("auth/register");
});

/* POST register processing. */
router.post('/register', function(req, res, next) {
 	var formData = req.body;console.log(formData);
 	const saltRounds = 10;
 	var salt = bcrypt.genSaltSync(saltRounds);
 	var hash = bcrypt.hashSync(formData.password, salt);
 	formData.password = hash;
models.User.create(formData).then(result=>{
	req.flash("info","Account Created Successfully");
	res.redirect("/auth/login");
})
});

module.exports = router;
