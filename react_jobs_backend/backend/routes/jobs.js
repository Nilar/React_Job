var express = require('express');
var router = express.Router();
var models = require("../models");

/* GO List page. */
router.get('/',async function(req, res, next) {
	
	let conditions = {
		include:[
		{
			model:models.Category
		},
		{
			model:models.User,
			attributes: { exclude: ['password','status'] }
		}]
	};
// ASYNC/AWAIT WAY
let jobs = await models.Job.findAll(conditions); 

res.render('jobs/index', { jobs: jobs });
});

/* GO Create page. */
router.get('/create',async function(req, res, next) {

	let cats = await models.Category.findAll();
	let agents = await models.User.findAll();

	res.render('jobs/create' , { categories : cats , agents:agents });
});

/* Store Category */
router.post('/', async function(req, res, next) {

	let formData = req.body;
	var errors = [];

	if(!formData.title){
		req.flash("errors","Title is required");
		errors.push("Title is required");
	}

	if(errors.length > 0){
		return res.redirect('/jobs/create');
	}

	let job = await models.Job.create(formData);
	res.redirect('/jobs');

});

/* @edit */
router.get('/edit/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let job = await models.Job.findOne({ where: {id: id} });
	let cats = await models.Category.findAll();
	let agents = await models.User.findAll();

	res.render('jobs/edit', { job: job ,categories : cats , agents:agents });
});

/* @update */
router.post('/update/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let formData = req.body;

	var errors = [];
	if(!formData.title){
		req.flash("errors","Title is required");
		errors.push("Title is required");
	}

	if(errors.length > 0){
		return res.redirect('/jobs/edit'+id);
	}

	let job = await models.Job.update(formData,{where:{id:id}});

	res.redirect('/jobs');
});

/* @delete */
router.post('/delete/:id', async function(req, res, next) {

	let id = req.params.id || 0;

	var errors = [];
	if(!id){
		req.flash("errors","Title is required");
		errors.push("Title is required");
	}

	if(errors.length > 0){
		return res.redirect('/jobs/edit'+id);
	}

	let category = await models.Job.destroy({where:{id:id}});
	res.redirect('/jobs');
});

module.exports = router;
