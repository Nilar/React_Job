var express = require('express');
var router = express.Router();
var models = require("../models");

/* GO List page. */
router.get('/',async function(req, res, next) {
	let conditions = {
		include:[
		{
			model:models.Category
		},
		{
			model:models.User,
			attributes: { exclude: ['password','status'] }
		}]
	};
	let resumes = await models.Resume.findAll(conditions);
	res.render('resumes/index', { data: resumes });
});
/* GO Create page. */
router.get('/create',async function(req, res, next) {

	let cats = await models.Category.findAll();
	let agents = await models.User.findAll();

	res.render('resumes/create' , { categories : cats , agents:agents });
});
/* Store Resumes */
router.post('/', async function(req, res, next) {

	let formData = req.body;
	var errors = [];

	if(!formData.title){
		req.flash("errors","Title is required");
		errors.push("Title is required");
	}

	if(errors.length > 0){
		return res.redirect('/resumes/create');
	}

	let resume = await models.Resume.create(formData);
	res.redirect('/resumes');

});
/* @edit */
router.get('/edit/:id', async function(req, res, next) {
      
	let id = req.params.id || 0;
	let resume = await models.Resume.findOne({ where: {id: id} });
	let cats = await models.Category.findAll();
	let agents = await models.User.findAll();
 
	res.render('resumes/edit', { resume: resume ,categories : cats , agents:agents });
});
/* @update */
router.post('/update/:id', async function(req, res, next) {

	let id = req.params.id || 0;
	let formData = req.body;

	var errors = [];
	if(!formData.title){
		req.flash("errors","Title is required");
		errors.push("Title is required");
	}

	if(errors.length > 0){
		return res.redirect('/jobs/edit'+id);
	}

	let resume = await models.Resume.update(formData,{where:{id:id}});

	res.redirect('/resumes');
});
/* @delete */
router.post('/delete/:id', async function(req, res, next) {

	let id = req.params.id || 0;

	var errors = [];
	if(!id){
		req.flash("errors","Title is required");
		errors.push("Title is required");
	}

	if(errors.length > 0){
		return res.redirect('/resumes/edit'+id);
	}

	let resume = await models.Resume.destroy({where:{id:id}});
	res.redirect('/resumes');
});

module.exports = router;
