require('dotenv').config();
var express = require('express');
var expressLayouts = require('express-ejs-layouts');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var flash = require('connect-flash');
var connectEnsureLogin = require('connect-ensure-login');
var JwtStrategy = require('passport-jwt').Strategy,
ExtractJwt = require('passport-jwt').ExtractJwt;
var passport = require('passport'); //
var Strategy = require('passport-local').Strategy; // check with username /password
var models = require('./models');
var users = require('./routes/auth');
var index = require('./routes/index');
var jobs = require('./routes/jobs');
var categories = require('./routes/categories');
var resumes = require('./routes/resumes');
var employers = require('./routes/employers');
var jobseekers = require('./routes/jobseekers');
var bcrypt = require('bcrypt');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);

passport.use(new Strategy({
    usernameField : 'email',
  },function(username, password, done) {
    console.log("checking credential");
    models.User.findOne({ where:{ email: username } }).then  (user => {
      console.log(user);
     
      if (!user) { return done(null, false,"No User Found"); }
      if (!bcrypt.compareSync(password,user.password)) { return done(null, false,'Incorrect Password'); }

      //if password is ok
      return done(null, user);
    });
  }

));

passport.serializeUser(function(user,cb){
  console.log("serial");
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  models.User.findOne({where : {id :id}}).then(user => {
    cb(null, user);
  });
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'keyboard cat',resave: false,saveUninitialized: true
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

//flash handler
app.use(function(req,res,next){
	res.locals.title = "My Default Title";
	res.locals.infos = req.flash('infos');
	res.locals.warnings = req.flash('warnings');
  res.locals.error = req.flash('error');
  res.locals.errors = req.flash('errors');
  res.locals.isLoggedIn = (req.user)?req.user:false;
	next();
})


app.use('/auth', users);
app.use('/',connectEnsureLogin.ensureLoggedIn('auth/login') ,index);
app.use('/jobs', connectEnsureLogin.ensureLoggedIn('auth/login') ,jobs);
app.use('/categories', connectEnsureLogin.ensureLoggedIn('auth/login') ,categories);
app.use('/resumes', connectEnsureLogin.ensureLoggedIn('auth/login') ,resumes);
app.use('/employers', connectEnsureLogin.ensureLoggedIn('auth/login') ,employers);
app.use('/jobseekers',connectEnsureLogin.ensureLoggedIn('auth/login') ,jobseekers);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
