var express = require('express');
var cors = require('cors');
var router = express.Router();
var models = require("../models");
var config = require("../config/config");
var Sequelize = require("sequelize");
var Op = Sequelize.Op;

/* @index */
router.get('/', async function(req, res, next) {

let conditions = {
                          include:[
                                        {
                                        model:models.Category
                                        },
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                                        }]
                                };
//pagination
let current_page = (req.query.page)?req.query.page:1;
let per_page = 2
conditions.limit = per_page;
conditions.offset = (current_page - 1)* per_page;

conditions.where ={};

let category = req.query.category;
if(category){
  /*conditions.where = {category_id:category};*/
  conditions.where.category_id = category;
}

let keyword = req.query.keyword;
if(keyword){
   conditions.where.title = {[Op.like]:"%"+keyword +"%"};
}

let results = await models.Resume.findAndCountAll(conditions);
let resumes = results.rows;
let total = results.count;
let total_pages = Math.ceil(total/per_page);

    res.json({
        success:true,
        message:'Resumes retrived successfully',
        resumes:resumes,
        total_pages : total_pages,
        current_page : current_page
    });

});

/* @show */
router.get('/:id', async function(req, res, next) {
   
   let id = req.params.id || 0;
   let resume = await models.Resume.findOne({ 
   							 include:[
                                        {
                                        model:models.Category
                                        },
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                                        }],
   									where: {id: id} 
   								});
    res.json({
        success:true,
        message:'Resume retrived successfully',
        resume:resume
    });
});

/* @store */
router.post('/', async function(req, res, next) {

   let formData = req.body;
   console.log(formData);
   if((!formData.title) && (!formData.description) && (!formData.user_id) && (!formData.how_to_contact) && (!formData.category_id)){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let resume = await models.Resume.create(formData);
   
    res.status(200).json({
        success:true,
        message:'Resume created successfully',
        resume:resume
    });
});

/* @update */
router.put('/:id', async function(req, res, next) {

   let id = req.params.id || 0;
   let formData = req.body;
   console.log(formData);
    if((!id) && (!formData.title) && (!formData.description) && (!formData.user_id) && (!formData.how_to_contact) && (!formData.category_id)){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let resume = await models.Resume.update(formData,{where:{id:id}});
   
    res.status(200).json({
        success:true,
        message:'Resume Update successfully',
        resume:resume
    });
});

/* @delete */
router.delete('/:id', async function(req, res, next) {

   let id = req.params.id || 0;
   
   if(!id){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let resume = await models.Resume.destroy({where:{id:id}});
   
    res.status(200).json({
        success:true,
        message:'Resume Deleted successfully',
        resume:resume
    });
});

/* @resume top */
router.get('/:resumetop', async function(req, res, next) {
   
   let id = req.params.id || 0;
   let resume = await models.Resume.findOne({ 
                 include:[
                                        {
                                        model:models.Category
                                        },
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                                        }],
                   limit:6
                  });
    res.json({
        success:true,
        message:'Resume retrived successfully',
        resume:resume
    });
});


module.exports = router;