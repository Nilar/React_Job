var express = require('express');
var router = express.Router();
var models = require("../models");
var passport = require('passport');
var bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');

/* @index */
router.get('/', async function(req, res, next) {

// ASYNC/AWAIT WAY
let employers = await models.Employer.findAll({
                          include:[
                                        
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                                        }]
                                });

    res.json({
        success:true,
        message:'employers retrived successfully',
        employers:employers
    });

});

/* POST user login. */
router.post('/login', function(req,res,next){
 
  passport.authenticate('local', {session: false}, (err, user,info) => {
   
      if(err || !user){
        return res.status(400).json;({
          message : 'Something is not right',
          user : user
        });
      }
    req.login(user, {session: false},async (err) => {
      
      if(err){
        res.send(err);
      }
      const token =  jwt.sign({ id : user.id,name: user.name}, 'YouDonotKnow');

      if(user.role == 0){ //jobseeker
          user = await models.User.findOne({
            where: {id: user.id} ,
            include:[{
                model: models.Jobseeker
            }]
          })
      }else if(user.role == 1) { // employer
          user = await models.User.findOne({
            where : { id : user.id},
            include:[{
              model: models.Employer
            }]
          })
      }

      return res.json({user,token});
    })

    
  })(req,res);

});

router.get('/profile', passport.authenticate('jwt', { session : false}),async function(req,res,next){
    let user = req.user;
    if(user.role == "0"){ //jobseeker

      user = await models.User.findOne({ 
        where : {id:user.id},
        include:[{ model : models.Jobseeker }]
      })

    //  delete user.password;
    }else if(user.role == "1"){ //employer
      user = await models.User.findOne({ 
        where : {id:user.id},
          include:[{ model : models.Employer
          }]
      });
     // delete user.password;
    }
    let u = user;
    
   return res.json({ user:u});
});



router.post('/logout', passport.authenticate('jwt', { session : false}), function(req,res,next){
  req.logout();
  return res.json({ message : "Logout Successfully"});
});




/* @Post User Register */
router.post('/register', async function(req, res, next) {

   let formData = req.body;
   
   if((!formData.name) && (!formData.phone) && (!formData.email)){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   formData.password = bcrypt.hashSync(formData.password, saltRounds);
   formData.role = models.User.Employer; // employer

   let user = await models.User.create(formData);

   if(user){
     formData.user_id = user.id;
     models.Employer.create(formData);
   }
   
    res.status(200).json({
        success:true,
        message:'User created successfully',
        user:user
    });
});


/* POST user forgotpassword. */
router.post('/forgot_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* POST user forgotpassword. */
router.post('/reset_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* GET user profile. */
router.post('/profile', passport.authenticate('jwt', { session : false}), async function(req,res,next){

  let user_id = req.user.id; //login user
  let formData = req.body;
  
    if(formData.password && formData.confirm_password.length >5){
      console.log("change password");
      formData.password = bcrypt.hashSync(formData.password, 10);
    }else{
      delete formData.password; // remove from data object
    }
  let u = await models.User.update(formData,{where : {id: user_id}});

  let e = await models.Employer.update(formData,{where : {id: user_id}});
  

res.json({
      success:true,
      message:'Employer Update successfully',
     // user:u
  });
});


module.exports = router;