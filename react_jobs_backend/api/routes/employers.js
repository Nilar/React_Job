var express = require('express');
var router = express.Router();
var models = require("../models");
var passport = require('passport');
var bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
var multer  = require('multer');
var upload = multer({ dest: 'public/uploads/' });
/* @index */
router.get('/', async function(req, res, next) {

// ASYNC/AWAIT WAY
let employers = await models.Employer.findAll({
                    include:[
                                  
                              {
                                  model:models.User,
                                  attributes: { exclude: ['password','status'] }
                              }], 
                              limit:20
                          });

    var host = req.get('host');

    employers.map((employer,i)=>{
     if(employer.company_logo){
        employer.company_logo = "http://"+host+"/uploads/"+employer.company_logo;
      }
    })
                    

    res.json({
        success:true,
        message:'employers retrived successfully',
        employers:employers
    });

});

/* POST user login. */
/*router.post('/login',passport.authenticate('local', { failureRedirect: '/auth/login' , failureFlash:true }) , function(req, res, next) {

  if(err || !user){
    return res.status(400).json({
      message: "Something is not right",
      user : user
    })
  }

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});*/

router.post('/login',function(req,res,next){ 
  passport.authenticate('local',{session:false},(err,user,info)=>{
    if(err || !user){
      return res.status(400).json({
          message : info || "Something is not right",
          user : user
      });
    }

    req.login(user,{session:false},async (err)=>{
      if(err){
        res.send(err);
      }
      const token = jwt.sign({id:user.id,name:user.name},'secret');

      if(user.role == 0)
      {
        user = await models.User.findOne({
          where : { id: user.id},
          include:[{
              model: models.Jobseeker
          }]
        });
      }
      else if (user.role == 1) 
      {
        user = await models.User.findOne({
          where : { id: user.id},
          include:[{
              model: models.Employer
          }]
        });
      }
      return res.json({user,token});
    });

  })(req,res);

});



/* @Post User Register */
router.post('/register', async function(req, res, next) {

   let formData = req.body;
   //console.log(formData);
   if((!formData.name) && (!formData.phone) && (!formData.email)){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   formData.password = bcrypt.hashSync(formData.password, saltRounds);
   formData.role = models.User.Employer;

   let user = await models.User.create(formData);

   if(user){
     formData.user_id = user.id;
     models.Employer.create(formData);
   }
   
    res.status(200).json({
        success:true,
        message:'User created successfully',
        user:user
    });
});


/* POST user forgotpassword. */
router.post('/forgot_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* POST user forgotpassword. */
router.post('/reset_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* GET user profile. */
/*router.get('/profile/:id', async function(req, res, next) {

  let id = req.params.id || 0;
  let user = await models.User.findOne({ 
                         attributes: { exclude: ['password','status'] },
                          where: {id: id} 
                        });
  res.json({
      success:true,
      message:'User Profile retrived successfully',
      user:user
  });
});*/

router.get('/profile',passport.authenticate('jwt',{session:false}),async function(req,res,next){
  let user = req.user;

  if(user.role ==0){
    user = await models.User.findOne({
      where : { id : user.id},
      include : [{
        model : models.Jobseeker
      }]
    });
  }else if(user.role = 1){
    user = await models.User.findOne({
      where : { id : user.id},
      include : [{
        model : models.Employer
      }]
    });

  var host = req.get('host');

    if(user.Employer.company_logo){
      user.Employer.company_logo = "http://"+host+"/uploads/"+user.Employer.company_logo;
    }
  }

  delete  user.password; 
  
  return res.json({user:user});
});

router.post('/profile',passport.authenticate('jwt',{ session : false }),async function(req,res,next){
  let user_id = req.user.id;
  let user = req.user;
  let formData = req.body;
  delete formData.company_logo;
    if(formData.password && formData.confirm_password.length > 5){
      formData.password = bcrypt.hashSync(formData.password,10);
    }else{
      delete formData.password;
    }
    let u = await models.User.update(formData,{where:{id:user_id}});
    
    if(user.role == 0){ // jobseeker
      let e = await models.Jobseeker.update(formData,{where:{user_id:user_id}});  // to change jobseeker and employer
    }else if(user.role == 1){ //employer
      let e = await models.Employer.update(formData,{where:{user_id:user_id}});  // to change jobseeker and employer

    }
    res.json({
      success : true,
      message : 'User Update Successfully'
     
    });
});

router.post('/profile_photo',[passport.authenticate('jwt',{ session : false }),upload.single('photo')],async function(req,res,next){
   // console.log(req.file);
    if(req.file){
      let user = req.user;
      if(user.role == 1){
        let e = await models.Employer.update({company_logo: req.file.filename},{where:{user_id:user.id}}); 
      }
  
    }
    res.json({
      success : true,
      message : 'Photo Upload successfully'
    });
});

router.post("/logout",passport.authenticate('jwt',{session:false}),function(req,res,next){
   req.logout();
   return res.json({message:"Logout successfully"});
})

module.exports = router;