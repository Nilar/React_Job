var express = require('express');
var cors = require('cors');
var router = express.Router();
var models = require("../models");
var config = require("../config/config");
var Sequelize = require("sequelize");
var Op = Sequelize.Op;
var passport = require('passport');
var multer  = require('multer');
var upload = multer({ dest: 'public/uploads/' });


/* @index */
router.get('/', async function(req, res, next) {

    let conditions = {
                        include:[
                                        {
                                        model:models.Category
                                        },
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                                        }]
                                };
    //pagination
    let current_page = (req.query.page)?req.query.page:1;
    let per_page = 2
    conditions.limit = per_page;
    conditions.offset = (current_page - 1)* per_page;

    conditions.where ={};

    let category = req.query.category;
    if(category){
    /*conditions.where = {category_id:category};*/
    conditions.where.category_id = category;
    }

    let keyword = req.query.keyword;
    if(keyword){
    conditions.where.title = {[Op.like]:"%"+keyword +"%"};
    }

    let results = await models.Resume.findAndCountAll(conditions);
    let resumes = results.rows;
    let total = results.count;
    let total_pages = Math.ceil(total/per_page);

    res.json({
        success:true,
        message:'Resumes retrived successfully',
        resumes:resumes,
        total_pages : total_pages,
        current_page : current_page
    });

});

router.get('/mine',passport.authenticate('jwt',{session:false}), async function(req, res, next) {
   
     let condition = {
                         include:[
                             {
                                 model: models.Category
                             },
                             {
                                 model : models.User,
                                 attributes : { exclude : ['password' , 'status']}
                             }
                         ],
                         order : [
                             ['created_at','desc']
                         ]
     }
 
     let current_page = (req.query.page) ? req.query.page : 1 ;
     let per_page = 1;
 
     condition_limit = per_page;
     condition.offset = (current_page - 1) * per_page ; // (2-1) * 5
     
     condition.where = {
         user_id : req.user.id
     }
 
     // Async/await
 
     let results = await models.Resume.findAndCountAll(condition);
    
     let resumes = results.rows;
     let total = results.count;
     let total_pages = Math.ceil(total/per_page);
 
     res.json({
         success : true,
         message : "My Resume retrived successfully",
         resumes : resumes,
         total_pages : total_pages,
         current_page : current_page
     })
 
 });
 
/* @show */
router.get('/:id', async function(req, res, next) {
   
   let id = req.params.id || 0;
   let resume = await models.Resume.findOne({ 
   							 include:[
                                        {
                                        model:models.Category
                                        },
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                                        }],
   									where: {id: id} 
   								});
    res.json({
        success:true,
        message:'Resume retrived successfully',
        resume:resume
    });
});

/* @store */
router.post('/',passport.authenticate('jwt',{session:false}), async function(req, res, next) {
    
    let formData = req.body;
   
    if((!formData.title) && (!formData.description) && (!formData.how_to_contact) && (!formData.category_id)){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
    }
    console.log(req.user.id);
    formData.user_id = req.user.id;
   let resume = await models.Resume.create(formData);
   
    res.status(200).json({
        success:true,
        message:'Resume created successfully',
        resume:resume
    });
});

/* @update */
router.put('/:id', async function(req, res, next) {

   let id = req.params.id || 0;
   let formData = req.body;
   console.log(formData);
    if((!id) && (!formData.title) && (!formData.description) && (!formData.user_id) && (!formData.how_to_contact) && (!formData.category_id)){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let resume = await models.Resume.update(formData,{where:{id:id}});
   
    res.status(200).json({
        success:true,
        message:'Resume Update successfully',
        resume:resume
    });
});

/* @delete */
router.delete('/:id', async function(req, res, next) {

   let id = req.params.id || 0;
   
   if(!id){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let resume = await models.Resume.destroy({where:{id:id}});
   
    res.status(200).json({
        success:true,
        message:'Resume Deleted successfully',
        resume:resume
    });
});

/* @resume top */
router.get('/:resumetop', async function(req, res, next) {
   
   let id = req.params.id || 0;
   let resume = await models.Resume.findOne({ 
                 include:[
                                        {
                                        model:models.Category
                                        },
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                                        }],
                   limit:6
                  });
    res.json({
        success:true,
        message:'Resume retrived successfully',
        resume:resume
    });
});



router.post('/uploadresumefile',[passport.authenticate('jwt',{ session : false }),upload.single('photo')],async function(req,res,next){
   // console.log(req.file); 
    if(req.file){
        if(!req.file.originalname.match(/\.(pdf|doc)$/)) {
            return cb(new Error('Only PDF and Word format are allowed!'), false);
        }else{
            let user = req.user;
            
                let e = await models.Resume.update({photo: req.file.filename},{where:{user_id:user.id}}); 
            
        }
    }
    res.json({
        success : true,
        message : 'Resume Upload successfully'
        
    });

})


module.exports = router;



