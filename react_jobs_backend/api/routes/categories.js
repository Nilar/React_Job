var express = require('express');
var router = express.Router();
var models = require("../models");

/* @index */
router.get('/', async function(req, res, next) {

// ASYNC/AWAIT WAY

let cats = await models.Category.findAll();

    res.json({
        success:true,
        message:'Catgories retrived successfully',
        categories:cats
    });

});

/* @show */
router.get('/:id', async function(req, res, next) {
   
   let id = req.params.id || 0;
   let category = await models.Category.findOne({ where: {id: id} });
    res.json({
        success:true,
        message:'Category retrived successfully',
        category:category
    });
});

/* @store */
router.post('/', async function(req, res, next) {

   let formData = req.body;
  
   if(!formData.name){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let category = await models.Category.create(formData);
   
    res.status(200).json({
        success:true,
        message:'Category created successfully',
        category:category
    });
});

/* @update */
router.put('/:id', async function(req, res, next) {

   let id = req.params.id || 0;
   let formData = req.body;
   console.log(formData);
   if(!id || !formData.name){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let category = await models.Category.update(formData,{where:{id:id}});
   
    res.status(200).json({
        success:true,
        message:'Category Update successfully',
        category:category
    });
});

/* @delete */
router.delete('/:id', async function(req, res, next) {

   let id = req.params.id || 0;
   
   if(!id){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let category = await models.Category.destroy({where:{id:id}});
   
    res.status(200).json({
        success:true,
        message:'Category Deleted successfully',
        category:category
    });
});

module.exports = router;
