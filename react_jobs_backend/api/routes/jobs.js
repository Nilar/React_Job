var express = require('express');
var cors = require('cors');
var router = express.Router();
var models = require("../models");
var config = require("../config/config");
var Sequelize = require("sequelize");
var Op = Sequelize.Op;
var passport = require('passport');

/* @index */
router.get('/', async function(req, res, next) {

    // PROMISE WAY
    // models.Job.findAll().then(jobs=>{
    //         console.log(jobs);
    // });
    let conditions = {
                    include:[
                                {
                                model:models.Category
                                },
                                {
                                    model:models.User,
                                    attributes: { exclude: ['password','status'] }
                                }],
                    order:[
                        ['created_at','desc']
                    ]
                };
    //pagination
    let current_page = (req.query.page)?req.query.page:1;
    let per_page = 5
    conditions.limit = per_page;
    conditions.offset = (current_page - 1)* per_page;


    conditions.where ={};

    let category = req.query.category;
    if(category){
    /*conditions.where = {category_id:category};*/
    conditions.where.category_id = category;
    }

    let keyword = req.query.keyword;
    if(keyword){
    conditions.where.title = {[Op.like]:"%"+keyword +"%"};
    }

    let employer_id = req.query.employer_id;
    if(employer_id){
    /*conditions.where = {employer_id_id:employer_id};*/
    conditions.where.agent_id = employer_id;
    }


    let sort_by = req.query.sort_by;
    if(sort_by){
    let sorts = ["asc","desc"];
    if(sorts.includes(sort_by)){
        conditions.order = [['created_at',sort_by]];
    } 
    }
    // ASYNC/AWAIT WAY
    let results = await models.Job.findAndCountAll(conditions);
    let jobs = results.rows;
    let total = results.count;
    let total_pages = Math.ceil(total/per_page);

        res.json({
            success:true,
            message:'Jobs retrived successfully',
            jobs:jobs,
            total_pages : total_pages,
            current_page : current_page
        });

});


router.get('/mine',passport.authenticate('jwt',{session:false}), async function(req, res, next) {


    let conditions = {
                          include:[
                                        {
                                        model:models.Category
                                        },
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                            }],
                          order:[
                              ['created_at','desc']
                            ]
                                };
    //pagination
    let current_page = (req.query.page)?req.query.page:1;
    let per_page = 5
    conditions.limit = per_page;
    conditions.offset = (current_page - 1)* per_page;

    conditions.where = {
    agent_id : req.user.id
    };


    // ASYNC/AWAIT WAY
    let results = await models.Job.findAndCountAll(conditions);
    let jobs = results.rows;
    let total = results.count;
    let total_pages = Math.ceil(total/per_page);

        res.json({
            success:true,
            message:'Jobs retrived successfully',
            jobs:jobs,
            total_pages : total_pages,
            current_page : current_page
        });

});


/* @store */
router.post('/',passport.authenticate('jwt',{session:false}), async function(req, res, next) {

   let formData = req.body;
   //console.log(formData);
   if(!formData.title){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   formData.agent_id = req.user.id;

   let job = await models.Job.create(formData);
   
    res.status(200).json({
        success:true,
        message:'Job created successfully',
        job:job
    });
});

/* @update */
router.put('/:id', async function(req, res, next) {

   let id = req.params.id || 0;
   let formData = req.body;
   console.log(formData);
   if(!id || !formData.title){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let job = await models.Job.update(formData,{where:{id:id}});
   
    res.status(200).json({
        success:true,
        message:'Job Update successfully',
        job:job
    });
});


/* @delete */
router.delete('/:id', async function(req, res, next) {

   let id = req.params.id || 0;
   
   if(!id){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   let job = await models.Job.destroy({where:{id:id}});
   
    res.status(200).json({
        success:true,
        message:'Job Deleted successfully',
        job:job
    });
});

/* @Recent Job */
router.get('/recent', cors(), async function(req, res, next) {

    // PROMISE WAY
        // models.Job.findAll().then(jobs=>{
        //         console.log(jobs);
        // });

    // ASYNC/AWAIT WAY
    let jobs = await models.Job.findAll({
                            include:[
                                            {
                                            model:models.Category
                                            },
                                            {
                                                model:models.User,
                                                attributes: { exclude: ['password','status'] }
                                            }],
                                            limit:6
                                    });

    let cats = await models.Category.findAll();

        res.json({
            success:true,
            message:'Recent Jobs retrived successfully',
            jobs:jobs,
            categories:cats
        });

});


/*salaries*/
router.get("/salaries",async function(req,res,next){
    let salaries = config.salaries;
    res.status(200).json({
      success:true,
      message:'',
      salaries:salaries
    })
})

/* @show */
router.get('/:id', async function(req, res, next) {
   
   let id = req.params.id || 0;
   let job = await models.Job.findOne({ where: {id: id} });
    res.json({
        success:true,
        message:'Job retrived successfully',
        job:job
    });
});



module.exports = router;
