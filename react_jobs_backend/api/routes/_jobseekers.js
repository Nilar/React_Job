var express = require('express');
var router = express.Router();
var models = require("../models");
var passport = require('passport');
var bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');

/* @index */
router.get('/', async function(req, res, next) {

// ASYNC/AWAIT WAY
let employers = await models.Employer.findAll({
                          include:[
                                        
                                        {
                                            model:models.User,
                                            attributes: { exclude: ['password','status'] }
                                        }]
                                });

    res.json({
        success:true,
        message:'employers retrived successfully',
        employers:employers
    });

});

/* POST user login. */
/*router.post('/login',passport.authenticate('local', { failureRedirect: '/auth/login' , failureFlash:true }) , function(req, res, next) {

  if(err || !user){
    return res.status(400).json({
      message: "Something is not right",
      user : user
    })
  }

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});*/

router.post('/login',function(req,res,next){ 
  passport.authenticate('local',{session:false},(err,user,info)=>{
    if(err || !user){
      return res.status(400).json({
          message : "Something is not right",
          user : user
      });
    }

    req.login(user,{session:false},(err)=>{
      if(err){
        res.send(err);
      }
      const token = jwt.sign({id:user.id,name:user.name},'secret');
      return res.json({user,token});
    });

  })(req,res);

});



/* @Post User Register */
router.post('/register', async function(req, res, next) {

   let formData = req.body;
   console.log(formData);
   if((!formData.name) && (!formData.phone) && (!formData.email)){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   formData.password = bcrypt.hashSync(formData.password, saltRounds);
   formData.role = models.User.SEEKER;

   let user = await models.User.create(formData);

   if(user){
     formData.user_id = user.id;
     models.Employer.create(formData);
   }
   
    res.status(200).json({
        success:true,
        message:'User created successfully',
        user:user
    });
});


/* POST user forgotpassword. */
router.post('/forgot_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* POST user forgotpassword. */
router.post('/reset_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* GET user profile. */
/*router.get('/profile/:id', async function(req, res, next) {

  let id = req.params.id || 0;
  let user = await models.User.findOne({ 
                         attributes: { exclude: ['password','status'] },
                          where: {id: id} 
                        });
  res.json({
      success:true,
      message:'User Profile retrived successfully',
      user:user
  });
});*/

router.get('/profile',passport.authenticate('jwt',{session:false}),function(req,res,next){
  return res.json({user:req.user});
})

router.post("/logout",passport.authenticate('jwt',{session:false}),function(req,res,next){
   req.logout();
   return res.json({message:"Logout successfully"});
})

module.exports = router;