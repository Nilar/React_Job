var express = require('express');
var router = express.Router();
var models = require("../models");
var passport = require('passport');
var bcrypt = require('bcrypt');
const saltRounds = 10;


/* POST user login. */
router.post('/login', passport.authenticate('jwt', { session: false }) , function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});



/* @Post User Register */
router.post('/register', async function(req, res, next) {

   let formData = req.body;

   if((!formData.name) && (!formData.phone) && (!formData.email)){
      return res.status(400).json({
        success : false,
        message : 'Validation error!'
      });
   }

   formData.password = bcrypt.hashSync(formData.password, saltRounds);

   let user = await models.User.create(formData);
   
    res.status(200).json({
        success:true,
        message:'User created successfully',
        user:user
    });
});


/* POST user forgotpassword. */
router.post('/forgot_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* POST user forgotpassword. */
router.post('/reset_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* GET user profile. */
router.get('/profile/:id', async function(req, res, next) {

  let id = req.params.id || 0;
  let user = await models.User.findOne({ 
                         attributes: { exclude: ['password','status'] },
                          where: {id: id} 
                        });
  res.json({
      success:true,
      message:'User Profile retrived successfully',
      user:user
  });
});


module.exports = router;
