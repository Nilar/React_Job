var express = require('express');
var cors = require('cors');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('dotenv').config();
var models = require("./models");
var bcrypt = require('bcrypt');

var index = require('./routes/index');
var auth = require('./routes/auth');
var jobs = require('./routes/jobs');
var categories = require('./routes/categories');
var resumes = require('./routes/resume');
var employers = require('./routes/employers');
var jobseekers = require('./routes/jobseekers');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';
/*opts.issuer = 'http://localhost:2000/';
opts.audience = 'http://localhost:4200/';*/

passport.use(new JwtStrategy(opts,function(jwt_payload,done){
  console.log('jwt');
  models.User.findOne({where : {id : jwt_payload.id}}).then(function(user){

    if(user){
      return done(null,user);
    }else{
      return done(null,false);
    }
  }).catch(function(err){
    return done(err,false);
  });
}))

function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

passport.use(new LocalStrategy({
  usernameField : 'email',
},  function(username, password, done) { console.log("checking credentials");
  
  if(isEmail(username)){ //console.log(username);
    var where = { email : username }
  }else{
     var where = { phone :username }
  }

    models.User.findOne({ where: where }).then(user=>{
      
      if (!user){ return done(null,false,'No user found!'); }
      if (!bcrypt.compareSync(password,user.password)){ return done(null,false,'Incorrect password'); }
      return done(null, user);
    });
  }

));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  models.User.findOne({where:{id:id}}).then(user => {
    cb(null,user);
  });
});

var app = express();

// view engine setup
//app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

// routes 
app.use('/api/', index);
app.use('/api/auth', auth);
app.use('/api/jobs',jobs);
app.use('/api/categories',categories);
app.use('/api/resumes',resumes);
app.use('/api/employers',employers);
app.use('/api/jobseekers',jobseekers);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
 // res.locals.message = err.message;
  //res.locals.error = req.app.get('env') === 'development' ? err : {};
  
  // render the error page
  res.status(err.status || 500);
  res.json({message:err.message||'Oops!',status: err.status});
//  res.render('error');
});

module.exports = app;
