'use strict';

module.exports = (sequelize, DataTypes) => {

  var Job = sequelize.define('Job', {
    title: DataTypes.STRING,
    salary:DataTypes.STRING,
    description: DataTypes.TEXT,
    how_to_apply:DataTypes.TEXT,
    category_id:DataTypes.INTEGER,
    agent_id:DataTypes.INTEGER,
  }, 
  
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });

  Job.associate = function (models) {

    models.Job.belongsTo(models.Category, {
      onDelete: "CASCADE",
      foreignKey: "category_id",
    });

    models.Job.belongsTo(models.User, {
      onDelete: "CASCADE",
      foreignKey: "agent_id",
    });

  };


  return Job;
};
