'use strict';

module.exports = (sequelize, DataTypes) => {

  var Jobseeker = sequelize.define('Jobseeker', {
    career_title: DataTypes.STRING,
    gender:DataTypes.INTEGER,
    birthday:DataTypes.DATE,
    user_id:DataTypes.INTEGER
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });

  Jobseeker.associate = function(models) {
    models.Jobseeker.belongsTo(models.User,
      {onDelete:"CASCADE",
      foreignKey: "user_id"});
   
  };


  return Jobseeker;
};
