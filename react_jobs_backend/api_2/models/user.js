'use strict';

module.exports = (sequelize, DataTypes) => {

  var User = sequelize.define('User', {
    name: DataTypes.STRING,
    phone:DataTypes.STRING,
    email:DataTypes.STRING,
    password: DataTypes.STRING,
    role:DataTypes.INTEGER,
    status:DataTypes.INTEGER
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });

  User.associate = function(models) {
    models.User.hasMany(models.Job,{foreignKey: "agent_id"});
    models.Resume.hasMany(models.Resume,{foreignKey: "user_id"});
  };


  return User;
};
