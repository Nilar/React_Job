'use strict';

module.exports = (sequelize, DataTypes) => {

  var Category = sequelize.define('Category', {
    name: DataTypes.STRING,
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });

  Category.associate = function(models) {
    models.Category.hasMany(models.Job,{foreignKey: "category_id"});
  };

  return Category;
};
