'use strict';

module.exports = (sequelize, DataTypes) => {

  var Employer = sequelize.define('Employer', {
    company_name: DataTypes.STRING,
    company_logo: DataTypes.STRING,
    company_address:DataTypes.STRING,
    about:DataTypes.TEXT,
    total_jobs:DataTypes.INTEGER,
    user_id:DataTypes.INTEGER,
    
  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  

  Employer.associate = function (models) {

    models.Employer.belongsTo(models.User, {
      onDelete: "CASCADE",
      foreignKey: "user_id",
    });

  };


  return Employer;
};
