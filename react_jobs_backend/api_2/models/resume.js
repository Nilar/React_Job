'use strict';

module.exports = (sequelize, DataTypes) => {

  var Resume = sequelize.define('Resume', {
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    how_to_contact:DataTypes.TEXT,
    category_id:DataTypes.INTEGER,
    user_id:DataTypes.INTEGER,
    photo:DataTypes.STRING

  },
  {
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  

  Resume.associate = function (models) {

    models.Resume.belongsTo(models.Category, {
      onDelete: "CASCADE",
      foreignKey: "category_id",
    });

    models.Resume.belongsTo(models.User, {
      onDelete: "CASCADE",
      foreignKey: "user_id",
    });

  };


  return Resume;
};
