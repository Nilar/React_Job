module.exports = {
    
    roles: ["Seeker","Agent","Administrator"],
    salaries : [
        {
            label : "0 - 100,000 Ks",
            value : [0,100000]
        },
        {
            label : "100,000 - 200,000 Ks",
            value : [100000,200000]
        },
        {
            label : "200,000 - 500,000 Ks",
            value : [200000,500000]
        },
        {
            label : "500,000 Ks - Above",
            value : [500000,0]
        }
    ]

};
  