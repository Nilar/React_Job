var express =  require('express');
var router = express.Router();
var models =  require("../models");

/* Get Resume Listing*/

router.get('/', async function(req , res , next) {

    let employers = await models.Employer.findAll({ limit: 6,
        include:[
            {
                model:models.User,
                attributes: { exclude: ['password','status'] }
               
            }]
        });

     res.json({
        success : true,
        message:'Employers retrived successfully',
        employers : employers
     });
});

/* Retrieved by id*/

// router.get('/:id', async function (req , res , next) {
//     var id = req.params.id || 0;

//     if(!id){
//         return res.status(400).json({
//             success : false,
//             message: 'Validation error!'
//         })
//     }
//     let status = await models.Resume.findOne({ where : { id: id}});

//      res.json({
//         success : true,
//         message:'Resume retrived successfully',
//         resume:status
//      });
// });

// /* Store */

// router.post('/', async function (req , res , next) {

//     var formData = req.body;

//     if(!formData){
//         return res.status(400).json({
//             success : false,
//             message: 'Validation error!'
//         })
//     }

//     let status = await models.Resume.create(formData);

//      res.json({
//         success : true,
//         message:'Resume created successfully',
//         resume:status
//      });
// });

// /* Update */

// router.put('/:id', async function (req , res , next) {

//     var formData = req.body;
//     var id = req.params.id || 0;
//     if(!formData || !id){
//         return res.status(400).json({
//             success : false,
//             message: 'Validation error!'
//         })
//     }

//     let status = await models.Resume.update(formData, { where : { id : id}});

//         res.json({
//         success : true,
//         message:'Resume updated successfully',
//         resume:status
//         });
// });

// /* Delete */

// router.delete('/:id', async function (req , res , next) {

//     var id = req.params.id || 0;
//     if(!id){
//         return res.status(400).json({
//             success : false,
//             message: 'Validation error!'
//         })
//     }

//     let status = await models.Resume.destroy({ where : { id : id}});

//         res.json({
//         success : true,
//         message:'Resume deleted successfully',
//         delete_status:status
//         });
// });

module.exports = router;