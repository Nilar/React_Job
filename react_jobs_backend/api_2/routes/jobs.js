var express = require('express');
var router = express.Router();
var models = require("../models");
var config = require("../config/config");


/* GET jobs listing. */
router.get('/', async function(req, res, next) {
   
        // PROMISE WAY
            // models.Job.findAll().then(jobs=>{
            //         console.log(jobs);
            // });

        // ASYNC/AWAIT WAY
        let conditions ={
            include:[
                {
                model:models.Category
                },
                {
                    model:models.User,
                    attributes: { exclude: ['password','status'] }
                }]
            };

        let category = req.query.category;

        if(category){
            conditions.where = { category_id :category};
        }

        let sort_by = req.query.sort_by;
        if(sort_by){
            let sorts = ["asc","desc"];
            if(sorts.includes(sort_by)){
                conditions.order = [['created_at', sort_by ]];
            }
            
        }
        let jobs = await models.Job.findAll(conditions);

        let cats = await models.Category.findAll();

            res.json({
                success:true,
                message:'Jobs retrived successfully',
                jobs:jobs,
                categories:cats
            });

});
/* @salary */

router.get('/salaries', function(req,res, next){
    let salaries = config.salaries;
    res.status(200).json({
        success : true,
        message : '',
        salaries : salaries
    })
 })
/* Recent Job */
router.get('/:recent', async function(req, res, next) {
    
    let jobs = await models.Job.findAll({ limit: 6,
        include:[
            {
            model:models.Category
            },
            {
                model:models.User,
                attributes: { exclude: ['password','status'] }
            }]
        });
  
     res.json({
         success:true,
         message:'Recent Job successfully',
         jobs:jobs
     });
 });

 
 
/* Show */

router.get('/:id', async function(req, res, next) {
   
   let id = req.params.id || 0;
   let status = await models.Job.findOne({ where: {id: id} });
    res.json({
        success:true,
        message:'Job Show successfully',
        job:status
    });
});

/* @store */

router.post('/', async function(req, res, next) {
   
    let formData =  req.body;
    //console.log(formData);
    if(!formData.title){
        return res.status(400).json({
            success: false,
            message: 'Validation error!'
        });
    }

    let status = await models.Job.create(formData);
    
    return res.status(200).json({
         success:true,
         message:'Job created successfully',
         job:status
     });
 });
/* @Update */
 router.put('/:id', async function(req, res, next) {
    let id = req.params.id || 0;
    let formData =  req.body;
    console.log(formData);
    if(!formData.title || !id){
        return res.status(400).json({
            success: false,
            message: 'Validation error!'
        });
    }

    let job = await models.Job.update(formData,{ where : { id : id}});
    
    return res.status(200).json({
         success:true,
         message:'Job updated successfully',
         updated_status:status
     });
 });
 

 /* @Delete */
 router.delete('/:id', async function(req, res, next) {
    let id = req.params.id || 0;
    
    if(!id){
        return res.status(400).json({
            success: false,
            message: 'Validation error!'
        });
    }

    let status = await models.Job.destroy({ where : { id : id}});
    
    return res.status(200).json({
         success:true,
         message:'Job deleted successfully',
         delete_status:status
     });
 });

 
 
module.exports = router;
