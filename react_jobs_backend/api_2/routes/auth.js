var express = require('express');
var router = express.Router();

/* POST user login. */
router.post('/login', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* POST user register. */
router.post('/register', function(req, res, next) {

  res.json({
    success:true,
    message:'User created successfully',
    user:{}
  });
});

/* POST user forgotpassword. */
router.post('/forgot_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* POST user forgotpassword. */
router.post('/reset_password', function(req, res, next) {

  res.json({
    success:true,
    message:'User authenticated successfully',
    user:{}
  });
});

/* POST user forgotpassword. */
router.get('/profile', function(req, res, next) {

  res.json({
    success:true,
    message:'User profile retrived successfully',
    user:{}
  });
});


module.exports = router;
