var express = require('express');
var router = express.Router();
var models = require("../models");
var cors = require('cors');


/* GET categories listing. */
//router.get('/',cors(), async function(req, res, next) {
router.get('/', async function(req, res, next) {
   
    let cat = await models.Category.findAll();
   
    res.json({
        success:true,
        message:'Categories retrived successfully',
        categories :cat
    });
});

/* @store */

router.post('/', async function(req,res,next){

    let formData = req.body;

    if(!formData){
        return res.status(400).json({
            success : false,
            message : "Validation error!"
        });
    }

    let status = await models.Category.create(formData);

    return res.status(200).json({
        message : true,
        message : 'Categories created successfully',
        categories : status
    });

});


/* Get Category by Id */
router.get('/:id', async function(req,res,next){

    let id = req.params.id || 0;
    let cat = await models.Category.findOne({ where : { id:id }});
    res.json({
        success : true,
        message : "Categories successfully",
        categories : cat
    });
   
});

/* @Update */

router.put('/:id', async function(req,res,next){
    let id = req.params.id || 0;
    let formData = req.body;
    console.log(formData);
    if(!formData || !id){
        return res.status(400).json({
            success : false,
            message : "Validation error!"
        });
    }

    let status = await models.Category.update(formData, { where : { id : id }});

    return res.status(200).json({
        message : true,
        message : 'Category updated successfully',
        updated_status : status
    });

});

/* @Delete */
router.delete('/:id', async function(req, res, next) {
    let id = req.params.id || 0;
    
    if(!id){
        return res.status(400).json({
            success: false,
            message: 'Validation error!'
        });
    }

    let status = await models.Category.destroy({ where : { id : id}});
    
    return res.status(200).json({
         success:true,
         message:'Category deleted successfully',
         delete_status:status
     });
 });



module.exports = router;
